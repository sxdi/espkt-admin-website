<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Service\Models\Service as ServiceModels;

class AdminServiceDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminServiceDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function init()
    {
        $service = $this->getCurrent();
        $picture = $this->addComponent(
            'Responsiv\Uploader\Components\ImageUploader',
            'pictureUploader',
            [
                'deferredBinding' => true,
                'maxSize'         => '2',
            ]
        );
        $picture->bindModel('picture', ServiceModels::find($service->id));

        $formulir = $this->addComponent(
            'Responsiv\Uploader\Components\FileUploader',
            'formulirUploader',
            [
                'deferredBinding' => true,
                'maxSize'         => '2',
                'fileTypes'       => 'rtf'
            ]
        );
        $formulir->bindModel('formulir', ServiceModels::find($service->id));
    }

    public function onRun()
    {
        $service = $this->getCurrent();

        $this->page['service'] = $service;
    }

    public function getCurrent()
    {
        return ServiceModels::whereParameter($this->property('parameter'))->first();
    }

    public function onSave()
    {
        $rules = [
            'name'        => 'required',
            'description' => 'required',
            'day_open'    => 'required',
            'time_open'   => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'description' => 'konten',
            'day_open'    => 'hari hari buka',
            'time_open'   => 'waktu hari buka',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $service              = $this->getCurrent();
        $service->name        = post('name');
        $service->description = post('description');
        $service->day_open    = post('day_open');
        $service->time_open   = post('time_open');
        $service->save(null, post('_session_key'));

        Flash::success('Layanan berhasil disimpan');
        return Redirect::refresh();
    }
}
