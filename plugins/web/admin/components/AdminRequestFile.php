<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Renatio\DynamicPDF\Classes\PDF;

use Pulangpisau\Request\Models\Request as RequestModels;

class AdminRequestFile extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequestFile Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $request = $this->getCurrent();
        if(!$request) {

        }

        $templateCode = 'renatio::invoice'; // unique code of the template
        $data         = [
            'request' => $request
        ]; // optional data used in template
        return PDF::loadTemplate($templateCode, $data)->stream($request->code.'.pdf');
    }

    public function getCurrent()
    {
        return RequestModels::whereParameter($this->property('parameter'))->first();
    }
}
