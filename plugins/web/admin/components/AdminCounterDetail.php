<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Counter\Models\Counter as CounterModels;
use Pulangpisau\Counter\Models\CounterService as CounterServiceModels;
use Pulangpisau\Service\Models\Service as ServiceModels;

class AdminCounterDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminCounterDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $counter = $this->getCurrent();

        $this->page['counter'] = $counter;
    }

    public function getCurrent()
    {
        return CounterModels::whereParameter($this->property('parameter'))->first();
    }

    public function getService()
    {
        $counters = [];
        $counter  = $this->getCurrent();
        $counter  = CounterServiceModels::select('service_id')->whereHas('counter', function($q) use($counter) {
            $q->whereCounterId($counter->id);
        })->get();

        foreach ($counter as $k) {
            array_push($counters, $k->service_id);
        }

        return ServiceModels::whereNull('parent_id')->doesnthave('counters')->get();
    }

    public function onSave()
    {
        $rules = [
            'name'        => 'required',
            'code'        => 'required|unique:pulangpisau_counter_counters,code,'.$this->getCurrent()->id,
            'description' => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'code'        => 'kode',
            'description' => 'deskripsi',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $counter              = $this->getCurrent();
        $counter->name        = post('name');
        $counter->code        = post('code');
        $counter->description = post('description');
        $counter->save();

        Flash::success('Loket berhasil disimpan');
        return Redirect::refresh();
    }

    public function onSaveService()
    {
        $counter = $this->getCurrent();
        if(count(post('service_id'))) {
            $counter->services()->sync(post('service_id'));
        }
        else {
            $counter->services()->detach();
        }

        Flash::success('Layanan berhasil disimpan');
        return Redirect::refresh();
    }
}
