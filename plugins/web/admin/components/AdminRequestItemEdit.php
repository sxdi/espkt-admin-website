<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

class AdminRequestItemEdit extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequestItemEdit Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $request = $this->getCurrent();
        if($request->status != 'progress' && $request->status != 'waiting') {
            Flash::error('Permohonan tidak dapat diubah');
            return Redirect::back();
        }
    }

    public function getCurrent()
    {
        return RequestItemModels::whereParameter($this->property('parameter'))->first();
    }
}
