<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Pulangpisau\Service\Models\Service as ServiceModels;

use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

class AdminDashboard extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminDashboard Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getServiceByPermit($permit)
    {
        return ServiceModels::whereCode($permit)->first();
    }

    public function getRequestByPermit($permit, $limit)
    {
        $service = $this->getServiceByPermit($permit);
        return RequestItemModels::whereRequestableType($service->model)->take($limit)->orderBy('created_at', 'desc')->get();
    }

    /**
     * Action
     */
    public function onRefreshPermitHearseRequest()
    {
        $this->page['permitHearses'] = $this->getRequestByPermit('permit-hearse', 10);
    }
}
