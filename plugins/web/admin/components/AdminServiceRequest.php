<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Pulangpisau\Service\Models\Service as ServiceModels;

class AdminServiceRequest extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminServiceRequest Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return ServiceModels::whereNotNull('parent_id')->get();
    }
}
