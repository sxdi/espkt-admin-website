<?php namespace Web\Admin\Components;

use Hash;
use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\SessionManager;

use Pulangpisau\User\Models\User as UserModels;
use Pulangpisau\Officer\Models\Officer as OfficerModels;

class AdminPassword extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminPassword Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function onSave()
    {
        $session = new SessionManager();
        $user    = $this->getUser();
        $rules   = [
            'password_old'    => 'required',
            'password'        => 'required|min:6',
            'password_retype' => 'required|same:password',
        ];
        $messages       = [];
        $attributeNames = [
            'password_old'    => 'sandi lama',
            'password'        => 'sandi baru',
            'password_retype' => 'ulangi sandi',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        if(!Hash::check(post('password_old'), $user->password)) {
            Flash::error('Sandi lama tidak sesuai');
            return;
        }

        UserModels::whereId($user->id)->update([
            'password' => Hash::make(post('password'))
        ]);
        $session->destroy();
        Flash::success('Sandi berhasil diubah');
        return Redirect::refresh();
    }
}
