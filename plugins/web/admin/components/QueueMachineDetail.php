<?php namespace Web\Admin\Components;

use Flash;
use Redirect;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\SessionManager;

use Pulangpisau\Queue\Models\Queue;
use Pulangpisau\Counter\Models\Counter;

use Pulangpisau\Request\Models\Request as RequestModels;
use Pulangpisau\Request\Models\RequestQueue as RequestQueueModels;
use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

class QueueMachineDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'QueueMachineDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $counter = $this->getCurrent();
        if(!$counter) {
            Flash::error('Data tidak ditemukan');
            return Redirect::to('404');
        }


        $this->page->title           = 'Daftar antrian loket '.$counter->name;
        $this->page['queueWaitings'] = $this->getAllByStatusWaiting();
        $this->page['counter']       = $counter;
    }

    public function getCurrent()
    {
        return Counter::whereParameter($this->property('parameter'))->first();
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function getAllByStatusWaiting()
    {
        $counter = $this->getCurrent();
        return Queue::whereCounterId($counter->id)->whereStatus('waiting')->whereDate('created_at', 'like', date('Y-m-d'))->get();
    }

    public function getAllByStatusHandle()
    {
        $counter = $this->getCurrent();
        return Queue::whereCounterId($counter->id)->whereStatus('handle')->whereDate('created_at', 'like', date('Y-m-d'))->get();
    }



    /**
     * Action
    */
    public function onRefreshQueueWaiting()
    {
        $this->page['queueWaitings'] = $this->getAllByStatusWaiting();
        $this->page['userLogin']     = $this->getUser();
        return;
    }

    public function onRefreshQueueHandle()
    {
        $this->page['queueHandles'] = $this->getAllByStatusHandle();
        $this->page['userLogin']     = $this->getUser();
        return;
    }

    public function onCall()
    {
        $queue               = Queue::whereParameter(post('parameter'))->first();
        $this->page['queue'] = $queue;

        if($queue->number >= 10) {
            $this->page['value1'] = substr($queue->number, 0, 1);
            $this->page['value2'] = substr($queue->number, 1, 2);
        }

        return;
    }

    public function onHandle()
    {
        $user               = $this->getUser();
        $terminal           = $this->getCurrent();
        $queue              = Queue::whereParameter(post('parameter'))->first();
        $queue->status      = 'handle';
        $queue->terminal_id = $terminal->id;
        $queue->user_id     = $user->id;
        $queue->save();
        return;
    }

    public function onHandleCancel()
    {
        $queue              = Queue::whereParameter(post('parameter'))->first();
        $queue->status      = 'waiting';
        $queue->terminal_id = null;
        $queue->user_id     = null;
        $queue->save();
        return;
    }

    public function onHandleRequest()
    {
        $queue   = Queue::whereParameter(post('parameter'))->first();
        $request = RequestModels::whereHas('items', function($q) use($queue){
            $q->whereStatus('waiting');
            // $q->whereHas('counter', function($r) use($queue){
            //     $r->whereCounterId($queue->counter_id);
            // });
        })->orderBy('created_at', 'desc')->get();
        $this->page['queue']    = $queue;
        $this->page['requests'] = $request;
    }

    public function onHandleRequestUse()
    {
        /**
         * Queue
         * @var [type]
         */
        $queue        = Queue::whereParameter(post('queue'))->first();
        $queue->status= 'finish';
        $queue->save();

        /**
         * Request Item
         * @var [type]
         */
        $item         = RequestItemModels::whereParameter(post('request'))->first();
        $item->status = 'progress';
        $item->save();

        /**
         * Request Queue
         * @var [type]
         */
        $itemQueue = RequestQueueModels::firstOrCreate([
            'item_id'  => $item->id,
            'queue_id' => $queue->id
        ]);
        $itemQueue->save();

        return Redirect::to('permohonan/item/detail/'.$item->parameter);
    }
}
