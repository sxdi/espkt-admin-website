<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\SessionManager;
use Pulangpisau\Core\Classes\RequestPermitManager;

use Pulangpisau\Service\Models\Service as ServiceModels;

use Pulangpisau\Request\Models\Request as RequestModels;
use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

use Pulangpisau\Request\Models\RequestSkckPermit;

class AdminRequestPermitSkck extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequestPermitSkck Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function getService($id)
    {
        return ServiceModels::whereId($id)->first();
    }

    public function onSave()
    {
        $user          = $this->getUser();
        $service       = $this->getService(post('service_id'));
        $permitManager = new RequestPermitManager();

        $rules = [
            'name'        => 'required',
            'phone'       => 'required',
            'email'       => 'email',
            'gender'      => 'required|in:male,female',
            'job'         => 'required',
            'nationality' => 'required',
            'address'     => 'required',

            'type'   => 'required|in:new,extend',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'phone'       => 'no telepon',
            'email'       => 'email',
            'gender'      => 'jenis kelamin',
            'job'         => 'pekerjaan',
            'nationality' => 'kebangsaan',
            'address'     => 'alamat',

            'type'        => 'tipe',
        ];


        if(post('type') == 'extend') {
            $rules['hasSkck']          = 'required';
            $attributeNames['hasSkck'] = 'berkas sim';
        }

        $rules['hasKtp']          = 'required|in:1';
        $attributeNames['hasKtp'] = 'berkas ktp';
        $rules['hasKk']           = 'required|in:1';
        $attributeNames['hasKk']  = 'berkas kk';
        $rules['hasAkta']         = 'required|in:1';
        $attributeNames['hasAkta']= 'berkas akta kelahiran atau ijazah terakhir';

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        /**
         * Request
         * @var [type]
         */
        $request = RequestModels::firstOrCreate([
            'code' => input('origin_code')
        ]);
        $request->save();

        /**
         * Request Item
         * @var RequestItemModels
         */
        $requestItem                   = new RequestItemModels;
        $requestItem->request_id       = $request->id;
        $requestItem->requestable_type = $service->parent->model;
        $requestItem->service_id       = $service->id;
        $requestItem->service_name     = $service->name;
        $requestItem->user_id          = $user ? $user->id : '';
        $requestItem->status           = 'progress';
        $requestItem->save();

        /**
         * Request Permit
         * @var RequestSkckPermit
         */
        $permit            = new RequestSkckPermit;
        $permit->item_id   = $requestItem->id;
        $permit->type      = post('type');
        $permit->hasSkck   = post('hasSkck');
        $permit->hasKtp    = post('hasKtp');
        $permit->hasKk     = post('hasKk');
        $permit->hasAkta   = post('hasAkta');
        $permit->hasPaspor = post('hasPaspor');
        $permit->save();

        $requestItem->requestable_id = $permit->id;
        $requestItem->save();

        /**
         * If reference
         */
        if(input('ref')) {
            $permitManager->makeQueue($requestItem->id, input('ref'));
        }

        /**
         * Request Customer
         * @var RequestCustomer
         */
        $permitManager->makeRequester($requestItem->id, post());

        /**
         * Request Officer
         * @var RequestOfficer
         */
        $permitManager->makeOfficer($requestItem->id, post());

        Flash::success('Permohonan berhasil disimpan');
        return Redirect::to('permohonan/item/detail/'.$requestItem->parameter);
    }
}
