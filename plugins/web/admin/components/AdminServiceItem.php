<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Service\Models\Service as ServiceModels;
use Pulangpisau\Service\Models\ServiceItem as ServiceItemModels;

class AdminServiceItem extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminServiceItem Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {

        $this->page['services'] = $this->getService();
    }

    public function getAll()
    {
        return ServiceItemModels::orderBy('id', 'desc')->get();
    }

    public function getService()
    {
        return ServiceModels::whereNull('parent_id')->orderBy('name' , 'asc')->get();
    }

    public function onSave()
    {
        $rules = [
            'service_id' => 'required',
            'name'       => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'service_id' => 'layanan',
            'name'       => 'nama',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $serviceItem             = new ServiceItemModels;
        $serviceItem->service_id = post('service_id');
        $serviceItem->name       = post('name');
        $serviceItem->code       = strtolower(post('name'));
        $serviceItem->save(null, post('_session_key'));

        Flash::success('Item berhasil disimpan');
        return Redirect::refresh();
    }
}
