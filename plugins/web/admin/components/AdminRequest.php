<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Officer\Models\Officer as OfficerModels;
use Pulangpisau\Service\Models\Service as ServiceModels;

use Pulangpisau\Location\Models\Regency;

use Pulangpisau\Core\Classes\SessionManager;
use Pulangpisau\Core\Classes\RequestPermitManager;

use Pulangpisau\Queue\Models\Queue as QueueModels;

use Pulangpisau\Request\Models\Request as RequestModels;
use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

use Pulangpisau\User\Models\UserService as UserServiceModels;

use Pulangpisau\Request\Models\RequestSimPermit;
use Pulangpisau\Request\Models\RequestSkckPermit;
use Pulangpisau\Request\Models\RequestTnkbPermit;
use Pulangpisau\Request\Models\RequestDrugPermit;
use Pulangpisau\Request\Models\RequestCrowdPermit;
use Pulangpisau\Request\Models\RequestHearsePermit;

class AdminRequest extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequest Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $queue         = QueueModels::whereParameter(input('ref'))->first();

        if($queue) {
            $kk = ServiceModels::whereHas('counters', function($q) use($queue) {
                $q->whereCounterId($queue->counter_id);
            })->first();
            $this->page['services'] = $kk->childs;
        }
    }

    public function getService()
    {
        return ServiceModels::orderBy('name')->get();
    }

    public function getServiceById($id)
    {
        return ServiceModels::whereId($id)->first();
    }

    public function getOfficer()
    {
        return OfficerModels::has('user')->orderBy('name')->get();
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function getAllRequest()
    {
        $user     = $this->getUser();
        $services = [];

        foreach ($user->services as $service) {
            foreach ($service->service->childs as $child) {
                array_push($services, $child->id);
            }
        }

        return RequestModels::orderBy('created_at', 'desc')->whereHas('items', function($q) use($services){
            $q->whereIn('service_id', $services);
        })->get();
    }

    public function getAll()
    {
        return RequestModels::orderBy('created_at', 'desc')->get();
    }


    /**
     * Action
     * @return [type] [description]
     */
    public function onSave()
    {
        $user          = $this->getUser();
        $service       = $this->getServiceById(post('service_id'));
        $permitManager = new RequestPermitManager();

        $rules = [
            'name'        => 'required',
            'phone'       => 'required',
            'email'       => 'email',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'phone'       => 'no telepon',
            'email'       => 'email',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        /**
         * Request
         * @var [type]
         */
        $request = RequestModels::firstOrCreate([
            'code' => input('origin_code')
        ]);
        $request->save();

        /**
         * Request Item
         * @var RequestItemModels
         */
        $requestItem                   = new RequestItemModels;
        $requestItem->request_id       = $request->id;
        $requestItem->requestable_type = $service->parent->model;
        $requestItem->service_id       = $service->id;
        $requestItem->service_name     = $service->name;
        $requestItem->user_id          = $user ? $user->id : '';
        $requestItem->status           = 'finish';
        $requestItem->save();

        /**
         * Request Permit
         * @var RequestSimPermit
         */
        if($service->parent->code == 'permit-sim') {
            $permit          = new RequestSimPermit;
            $permit->item_id = $requestItem->id;
            $permit->save();
        }

        if($service->parent->code == 'permit-skck') {
            $permit          = new RequestSkckPermit;
            $permit->item_id = $requestItem->id;
            $permit->save();
        }

        if($service->parent->code == 'permit-tnkb') {
            $permit          = new RequestTnkbPermit;
            $permit->item_id = $requestItem->id;
            $permit->save();
        }

        if($service->parent->code == 'permit-drug') {
            $permit          = new RequestDrugPermit;
            $permit->item_id = $requestItem->id;
            $permit->save();
        }

        if($service->parent->code == 'permit-crowd') {
            $permit          = new RequestCrowdPermit;
            $permit->item_id = $requestItem->id;
            $permit->save();
        }

        if($service->parent->code == 'permit-hearse') {
            $permit          = new RequestHearsePermit;
            $permit->item_id = $requestItem->id;
            $permit->save();
        }

        $requestItem->requestable_id = $permit->id;
        $requestItem->save();

        /**
         * If reference
         */
        if(input('ref')) {
            $permitManager->makeQueue($requestItem->id, input('ref'));
        }

        /**
         * Request Customer
         * @var RequestCustomer
         */
        $permitManager->makeRequester($requestItem->id, post());

        /**
         * Request Officer
         * @var RequestOfficer
         */
        // $permitManager->makeOfficer($requestItem->id, post());

        Flash::success('Permohonan berhasil disimpan');
        return Redirect::to('permohonan/item/detail/'.$requestItem->parameter);
    }

    public function onAddOfficer()
    {
        return [
            'officer' => $this->renderPartial('request/form-officer-item', [
                'key'      => post('length'),
                'officers' => $this->getOfficer()
            ])
        ];
    }

    public function onRemoveOfficer()
    {
        $expertise = SpeakerSpeakerExpertise::whereParameter(post('parameter'))->first()->delete();
        return [
            'expertise' => $this->renderPartial('speaker/team-list-expertises', [
                'speakerExpertises'  => $this->getSpeakerExpertises()
            ]),
            'message' => 'Keahlian berhasil dihapus',
            'type' => 'success',
        ];
    }

    public function onSelectService()
    {
        $service = ServiceModels::whereId(post('service_id'))->first();

        if(post('service_id')) {
            $this->page['serviceRequest'] = $service;
            $this->page['service']        = $service->parent;
            $this->page['officers']       = $this->getOfficer();

            if($service->code == 'permit-hearse') {
                $this->page['origin_regencies']      = Regency::orderBy('name', 'asc')->whereProvinceId(62)->get();
                $this->page['destination_regencies'] = Regency::orderBy('name', 'asc')->whereProvinceId(62)->get();
            }
        }
    }
}
