<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Renatio\DynamicPDF\Classes\PDF;
use Novay\WordTemplate\WordTemplate;

use Pulangpisau\Request\Models\Request as RequestModels;

class AdminRequestResume extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequestResume Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $request = $this->getCurrent();
        if(!$request) {

        }

        $service = $request->items[0]->service->code;

        if($service == 'skck-new' or $service == 'skck-extend') {
            $templateCode = 'skck-new::form'; // unique code of the template
            $data         = [
                'request' => $request
            ]; // optional data used in template

            return PDF::loadTemplate($templateCode, $data)->stream('['.$request->code.']'.$request->items[0]->service->parent->name.' - '.$request->items[0]->service_name.'.pdf');
        }

        $file    = env('APP_URL').$request->items[0]->service->formulir->getPath();
        $sims    = ['sim-new', 'sim-extend', 'sim-student'];
        if(in_array($service, $sims)) {
            if($request->items[0]->requestable->hasPsy == 'no') {
                $has_psy = 'Tidak ada';
            }

            if($request->items[0]->requestable->hasPsy == 'hands') {
                $has_psy = 'Tangan';
            }

            if($request->items[0]->requestable->hasPsy == 'foot') {
                $has_psy = 'Kaki';
            }

            if($request->items[0]->requestable->hasPsy == 'hearing') {
                $has_psy = 'Pendengaran';
            }

            if($request->items[0]->requestable->hasPsy == 'body') {
                $has_psy = 'Badan';
            }
        }

        switch ($service) {
            case 'sim-new':
                $array = [
                    '[service]'           => strtoupper($request->items[0]->service_name),
                    '[kind]'              => strtoupper($request->items[0]->requestable->type),
                    '[name]'              => strtoupper($request->items[0]->customer->name),
                    '[phone]'             => strtoupper($request->items[0]->customer->phone),
                    '[gender]'            => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[nationality]'       => strtoupper($request->items[0]->customer->nationality),
                    '[number_id]'         => strtoupper($request->items[0]->customer->number_id),
                    '[number_pasport]'    => strtoupper($request->items[0]->customer->number_pasport),
                    '[number_kitas]'      => strtoupper($request->items[0]->customer->number_kitas),
                    '[place]'             => strtoupper($request->items[0]->customer->place),
                    '[dob]'               => strtoupper($request->items[0]->customer->dob),
                    '[job]'               => strtoupper($request->items[0]->customer->job),
                    '[address]'           => strtoupper($request->items[0]->customer->address),

                    '[height]'            => strtoupper($request->items[0]->requestable->height),
                    '[education]'         => strtoupper($request->items[0]->requestable->education),
                    '[father]'            => strtoupper($request->items[0]->requestable->father),
                    '[mother]'            => strtoupper($request->items[0]->requestable->mother),

                    '[address_city]'      => strtoupper($request->items[0]->requestable->address_city),
                    '[address_village]'   => strtoupper($request->items[0]->requestable->address_village),
                    '[address_rt]'        => strtoupper($request->items[0]->requestable->address_rt),
                    '[address_rw]'        => strtoupper($request->items[0]->requestable->address_rw),
                    '[address_pos]'       => strtoupper($request->items[0]->requestable->address_pos),

                    '[emergency_city]'    => strtoupper($request->items[0]->requestable->emergency_city),
                    '[emergency_village]' => strtoupper($request->items[0]->requestable->emergency_village),
                    '[emergency_rt]'      => strtoupper($request->items[0]->requestable->emergency_rt),
                    '[emergency_rw]'      => strtoupper($request->items[0]->requestable->emergency_rw),
                    '[emergency_pos]'     => strtoupper($request->items[0]->requestable->emergency_pos),

                    '[has_glasses]'       => strtoupper($request->items[0]->requestable->hasGlasses ? 'Ya' : 'Tidak'),
                    '[has_psy]'           => strtoupper($has_psy),
                    '[has_certified]'     => strtoupper($request->items[0]->requestable->hasCertified ? 'Ya' : 'Tidak'),
                ];
                break;

            case 'sim-extend':
                $array = [
                    '[service]'           => strtoupper($request->items[0]->service_name),
                    '[kind]'              => strtoupper($request->items[0]->requestable->type),
                    '[name]'              => strtoupper($request->items[0]->customer->name),
                    '[phone]'             => strtoupper($request->items[0]->customer->phone),
                    '[gender]'            => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[nationality]'       => strtoupper($request->items[0]->customer->nationality),
                    '[number_id]'         => strtoupper($request->items[0]->customer->number_id),
                    '[number_pasport]'    => strtoupper($request->items[0]->customer->number_pasport),
                    '[number_kitas]'      => strtoupper($request->items[0]->customer->number_kitas),
                    '[place]'             => strtoupper($request->items[0]->customer->place),
                    '[dob]'               => strtoupper($request->items[0]->customer->dob),
                    '[job]'               => strtoupper($request->items[0]->customer->job),
                    '[address]'           => strtoupper($request->items[0]->customer->address),

                    '[height]'            => strtoupper($request->items[0]->requestable->height),
                    '[education]'         => strtoupper($request->items[0]->requestable->education),
                    '[father]'            => strtoupper($request->items[0]->requestable->father),
                    '[mother]'            => strtoupper($request->items[0]->requestable->mother),

                    '[address_city]'      => strtoupper($request->items[0]->requestable->address_city),
                    '[address_village]'   => strtoupper($request->items[0]->requestable->address_village),
                    '[address_rt]'        => strtoupper($request->items[0]->requestable->address_rt),
                    '[address_rw]'        => strtoupper($request->items[0]->requestable->address_rw),
                    '[address_pos]'       => strtoupper($request->items[0]->requestable->address_pos),

                    '[emergency_city]'    => strtoupper($request->items[0]->requestable->emergency_city),
                    '[emergency_village]' => strtoupper($request->items[0]->requestable->emergency_village),
                    '[emergency_rt]'      => strtoupper($request->items[0]->requestable->emergency_rt),
                    '[emergency_rw]'      => strtoupper($request->items[0]->requestable->emergency_rw),
                    '[emergency_pos]'     => strtoupper($request->items[0]->requestable->emergency_pos),

                    '[has_glasses]'       => strtoupper($request->items[0]->requestable->hasGlasses ? 'Ya' : 'Tidak'),
                    '[has_psy]'           => strtoupper($has_psy),
                    '[has_certified]'     => strtoupper($request->items[0]->requestable->hasCertified ? 'Ya' : 'Tidak'),
                ];
                break;

            case 'sim-student':
                $array = [
                    '[service]'           => strtoupper($request->items[0]->service_name),
                    '[kind]'              => strtoupper($request->items[0]->requestable->type),
                    '[name]'              => strtoupper($request->items[0]->customer->name),
                    '[phone]'             => strtoupper($request->items[0]->customer->phone),
                    '[gender]'            => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[nationality]'       => strtoupper($request->items[0]->customer->nationality),
                    '[number_id]'         => strtoupper($request->items[0]->customer->number_id),
                    '[number_pasport]'    => strtoupper($request->items[0]->customer->number_pasport),
                    '[number_kitas]'      => strtoupper($request->items[0]->customer->number_kitas),
                    '[place]'             => strtoupper($request->items[0]->customer->place),
                    '[dob]'               => strtoupper($request->items[0]->customer->dob),
                    '[job]'               => strtoupper($request->items[0]->customer->job),
                    '[address]'           => strtoupper($request->items[0]->customer->address),

                    '[height]'            => strtoupper($request->items[0]->requestable->height),
                    '[education]'         => strtoupper($request->items[0]->requestable->education),
                    '[father]'            => strtoupper($request->items[0]->requestable->father),
                    '[mother]'            => strtoupper($request->items[0]->requestable->mother),

                    '[address_city]'      => strtoupper($request->items[0]->requestable->address_city),
                    '[address_village]'   => strtoupper($request->items[0]->requestable->address_village),
                    '[address_rt]'        => strtoupper($request->items[0]->requestable->address_rt),
                    '[address_rw]'        => strtoupper($request->items[0]->requestable->address_rw),
                    '[address_pos]'       => strtoupper($request->items[0]->requestable->address_pos),

                    '[emergency_city]'    => strtoupper($request->items[0]->requestable->emergency_city),
                    '[emergency_village]' => strtoupper($request->items[0]->requestable->emergency_village),
                    '[emergency_rt]'      => strtoupper($request->items[0]->requestable->emergency_rt),
                    '[emergency_rw]'      => strtoupper($request->items[0]->requestable->emergency_rw),
                    '[emergency_pos]'     => strtoupper($request->items[0]->requestable->emergency_pos),

                    '[has_glasses]'       => strtoupper($request->items[0]->requestable->has_glasses ? 'Ya' : 'Tidak'),
                    '[has_psy]'           => strtoupper($has_psy),
                    '[has_certified]'     => strtoupper($request->items[0]->requestable->has_certified ? 'Ya' : 'Tidak'),
                ];
                break;

            case 'skck-new':
                $array = [
                    '[name]'         => ucwords(strtolower($request->items[0]->customer->name)),
                    '[gender]'       => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[nationality]'  => ucwords(strtolower($request->items[0]->customer->nationality)),
                    '[religion]'     => ucwords(strtolower($request->items[0]->customer->religion)),
                    '[place]'        => ucwords(strtolower($request->items[0]->customer->place)),
                    '[dob]'          => ucwords(strtolower($request->items[0]->customer->dob)),
                    '[address]'      => ucwords(strtolower($request->items[0]->customer->address)),
                    '[job]'          => ucwords(strtolower($request->items[0]->customer->job)),
                    '[number_id]'    => ucwords(strtolower($request->items[0]->customer->number_id)),
                    '[number_kitas]' => ucwords(strtolower($request->items[0]->customer->number_kitas)),
                    '[purpose]'      => ucwords(strtolower($request->items[0]->requestable->purpose)),
                ];
                break;

            case 'skck-extend':
                $array = [
                    '[name]'         => ucwords(strtolower($request->items[0]->customer->name)),
                    '[gender]'       => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[nationality]'  => ucwords(strtolower($request->items[0]->customer->nationality)),
                    '[religion]'     => ucwords(strtolower($request->items[0]->customer->religion)),
                    '[place]'        => ucwords(strtolower($request->items[0]->customer->place)),
                    '[dob]'          => ucwords(strtolower($request->items[0]->customer->dob)),
                    '[address]'      => ucwords(strtolower($request->items[0]->customer->address)),
                    '[job]'          => ucwords(strtolower($request->items[0]->customer->job)),
                    '[number_id]'    => ucwords(strtolower($request->items[0]->customer->number_id)),
                    '[number_kitas]' => ucwords(strtolower($request->items[0]->customer->number_kitas)),
                    '[purpose]'      => ucwords(strtolower($request->items[0]->requestable->purpose)),
                ];
                break;

            case 'drug-new':
                $array = [
                    '[header]'   => $request->items[0]->requestable->header_doctor,
                    '[name]'     => ucwords(strtolower($request->items[0]->customer->name)),
                    '[place]'    => ucwords(strtolower($request->items[0]->customer->place)),
                    '[dob]'      => ucwords(strtolower($request->items[0]->customer->dob)),
                    '[job]'      => ucwords(strtolower($request->items[0]->customer->job)),
                    '[gender]'   => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[religion]' => ucwords(strtolower($request->items[0]->customer->religion)),
                    '[address]'  => ucwords(strtolower($request->items[0]->customer->address)),
                    '[purpose]'  => ucwords(strtolower($request->items[0]->requestable->purpose)),
                ];
                break;

            case 'drug-extend':
                $array = [
                    '[header]'   => $request->items[0]->requestable->header_doctor,
                    '[name]'     => ucwords(strtolower($request->items[0]->customer->name)),
                    '[place]'    => ucwords(strtolower($request->items[0]->customer->place)),
                    '[dob]'      => ucwords(strtolower($request->items[0]->customer->dob)),
                    '[job]'      => ucwords(strtolower($request->items[0]->customer->job)),
                    '[gender]'   => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[religion]' => ucwords(strtolower($request->items[0]->customer->religion)),
                    '[address]'  => ucwords(strtolower($request->items[0]->customer->address)),
                    '[purpose]'  => ucwords(strtolower($request->items[0]->requestable->purpose)),
                ];
                break;

            case 'hearse':
                $array = [
                    '[name]'                 => ucwords(strtolower($request->items[0]->customer->name)),
                    '[phone]'                => ucwords(strtolower($request->items[0]->customer->phone)),
                    '[gender]'               => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[job]'                  => ucwords(strtolower($request->items[0]->customer->job)),
                    '[nationality]'          => ucwords(strtolower($request->items[0]->customer->nationality)),
                    '[address]'              => ucwords(strtolower($request->items[0]->customer->address)),

                    '[origin_address]'       => ucwords(strtolower($request->items[0]->requestable->origin_address)),
                    '[origin_rt]'            => ucwords(strtolower($request->items[0]->requestable->origin_rt)),
                    '[origin_rw]'            => ucwords(strtolower($request->items[0]->requestable->origin_rw)),
                    '[origin_village]'       => ucwords(strtolower($request->items[0]->requestable->origin_village->name)),
                    '[origin_district]'      => ucwords(strtolower($request->items[0]->requestable->origin_district->name)),
                    '[origin_regency]'       => ucwords(strtolower($request->items[0]->requestable->origin_regency->name)),

                    '[destination_address]'  => ucwords(strtolower($request->items[0]->requestable->destination_address)),
                    '[destination_rt]'       => ucwords(strtolower($request->items[0]->requestable->destination_rt)),
                    '[destination_rw]'       => ucwords(strtolower($request->items[0]->requestable->destination_rw)),
                    '[destination_village]'  => ucwords(strtolower($request->items[0]->requestable->destination_village->name)),
                    '[destination_district]' => ucwords(strtolower($request->items[0]->requestable->destination_district->name)),
                    '[destination_regency]'  => ucwords(strtolower($request->items[0]->requestable->destination_regency->name)),
                ];
                break;

            case 'hearse-other':
                $array = [
                    '[name]'                 => ucwords(strtolower($request->items[0]->customer->name)),
                    '[phone]'                => ucwords(strtolower($request->items[0]->customer->phone)),
                    '[gender]'               => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[job]'                  => ucwords(strtolower($request->items[0]->customer->job)),
                    '[nationality]'          => ucwords(strtolower($request->items[0]->customer->nationality)),
                    '[address]'              => ucwords(strtolower($request->items[0]->customer->address)),

                    '[origin_address]'       => ucwords(strtolower($request->items[0]->requestable->origin_address)),
                    '[origin_rt]'            => ucwords(strtolower($request->items[0]->requestable->origin_rt)),
                    '[origin_rw]'            => ucwords(strtolower($request->items[0]->requestable->origin_rw)),

                    '[destination_address]'  => ucwords(strtolower($request->items[0]->requestable->destination_address)),
                    '[destination_rt]'       => ucwords(strtolower($request->items[0]->requestable->destination_rt)),
                    '[destination_rw]'       => ucwords(strtolower($request->items[0]->requestable->destination_rw)),
                ];
                break;

            case 'crowd':
                $array = [
                    '[name]'        => ucwords(strtolower($request->items[0]->customer->name)),
                    '[address]'     => ucwords(strtolower($request->items[0]->customer->address)),

                    '[date]'        => ucwords(strtolower($request->items[0]->requestable->date)),
                    '[time]'        => ucwords(strtolower($request->items[0]->requestable->time)),
                    '[place]'       => ucwords(strtolower($request->items[0]->requestable->place)),
                    '[kind]'        => ucwords(strtolower($request->items[0]->requestable->kind)),
                    '[participant]' => ucwords(strtolower($request->items[0]->requestable->participant))
                ];
                break;

            case 'crowd-firework':
                $array = [
                    '[name]'        => ucwords(strtolower($request->items[0]->customer->name)),
                    '[address]'     => ucwords(strtolower($request->items[0]->customer->address)),

                    '[date]'        => ucwords(strtolower($request->items[0]->requestable->date)),
                    '[time]'        => ucwords(strtolower($request->items[0]->requestable->time)),
                    '[purpose]'     => ucwords(strtolower($request->items[0]->requestable->purpose)),
                    '[qty]'         => ucwords(strtolower($request->items[0]->requestable->qty))
                ];
                break;

            case 'crowd-campaign':
                $array = [
                    '[name]'             => ucwords(strtolower($request->items[0]->customer->name)),
                    '[phone]'            => ucwords(strtolower($request->items[0]->customer->phone)),

                    '[campaign_group]'   => ucwords(strtolower($request->items[0]->requestable->campaign_group)),
                    '[campaign_address]' => ucwords(strtolower($request->items[0]->requestable->campaign_address)),
                    '[kind]'             => ucwords(strtolower($request->items[0]->requestable->kind)),
                    '[date]'             => ucwords(strtolower($request->items[0]->requestable->date)),
                    '[time]'             => ucwords(strtolower($request->items[0]->requestable->time)),
                    '[place]'            => ucwords(strtolower($request->items[0]->requestable->place)),
                    '[participant]'      => ucwords(strtolower($request->items[0]->requestable->participant)),
                    '[lead]'             => ucwords(strtolower($request->items[0]->requestable->lead)),
                    '[vehicle]'          => ucwords(strtolower($request->items[0]->requestable->vehicle)),
                    '[apk]'              => ucwords(strtolower($request->items[0]->requestable->apk)),
                ];
                break;

            case 'crowd-public':
                $array = [
                    '[name]'             => ucwords(strtolower($request->items[0]->customer->name)),
                    '[phone]'            => ucwords(strtolower($request->items[0]->customer->phone)),

                    '[kind]'             => ucwords(strtolower($request->items[0]->requestable->kind)),
                    '[date]'             => ucwords(strtolower($request->items[0]->requestable->date)),
                    '[time]'             => ucwords(strtolower($request->items[0]->requestable->time)),
                    '[place]'            => ucwords(strtolower($request->items[0]->requestable->place)),
                ];
                break;

            case 'lost-new':
                $array = [
                    '[name]'        => ucwords(strtolower($request->items[0]->customer->name)),
                    '[nationality]' => ucwords(strtolower($request->items[0]->customer->nationality)),
                    '[job]'         => ucwords(strtolower($request->items[0]->customer->job)),
                    '[address]'     => ucwords(strtolower($request->items[0]->customer->address)),

                    '[content]'     => ucwords(strtolower($request->items[0]->requestable->content)),
                    '[description]' => ucwords(strtolower($request->items[0]->requestable->description)),
                    '[date]'        => ucwords(strtolower($request->items[0]->requestable->date)),
                    '[time]'        => ucwords(strtolower($request->items[0]->requestable->time))
                ];
                break;

            case 'pick-sick':
                $array = [
                    '[name]'                 => ucwords(strtolower($request->items[0]->customer->name)),
                    '[phone]'                => ucwords(strtolower($request->items[0]->customer->phone)),
                    '[gender]'               => $request->items[0]->customer->gender == 'male' ? 'Laki-laki' : 'Perempuan',
                    '[job]'                  => ucwords(strtolower($request->items[0]->customer->job)),
                    '[nationality]'          => ucwords(strtolower($request->items[0]->customer->nationality)),
                    '[address]'              => ucwords(strtolower($request->items[0]->customer->address)),

                    '[origin_address]'       => ucwords(strtolower($request->items[0]->requestable->origin_address)),
                    '[origin_rt]'            => ucwords(strtolower($request->items[0]->requestable->origin_rt)),
                    '[origin_rw]'            => ucwords(strtolower($request->items[0]->requestable->origin_rw)),
                    '[origin_village]'       => ucwords(strtolower($request->items[0]->requestable->origin_village->name)),
                    '[origin_district]'      => ucwords(strtolower($request->items[0]->requestable->origin_district->name)),
                    '[origin_regency]'       => ucwords(strtolower($request->items[0]->requestable->origin_regency->name)),

                    '[destination_address]'  => ucwords(strtolower($request->items[0]->requestable->destination_address)),
                    '[destination_rt]'       => ucwords(strtolower($request->items[0]->requestable->destination_rt)),
                    '[destination_rw]'       => ucwords(strtolower($request->items[0]->requestable->destination_rw)),
                    '[destination_village]'  => ucwords(strtolower($request->items[0]->requestable->destination_village->name)),
                    '[destination_district]' => ucwords(strtolower($request->items[0]->requestable->destination_district->name)),
                    '[destination_regency]'  => ucwords(strtolower($request->items[0]->requestable->destination_regency->name)),
                ];
                break;

            default:
                $templateCode = 'renatio::invoice';
                break;
        }

        // $fileName = '['.rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).']'.$request->items[0]->service->parent->name.' - '.$request->items[0]->service_name.'.doc';
        $fileName = '['.$request->code.']'.$request->items[0]->service->parent->name.' - '.$request->items[0]->service_name.'.doc';
        $k        = new WordTemplate;

        return $k->export($file, $array, $fileName);
    }

    public function getCurrent()
    {
        return RequestModels::whereParameter($this->property('parameter'))->first();
    }
}
