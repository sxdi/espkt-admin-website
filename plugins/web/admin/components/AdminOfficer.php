<?php namespace Web\Admin\Components;

use Hash;
use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\Generator;
use Pulangpisau\Core\Classes\MailManager;

use Pulangpisau\User\Models\User as UserModels;
use Pulangpisau\User\Models\UserUserGroup as UserUserGroupModels;

use Pulangpisau\Officer\Models\Officer as OfficerModels;

class AdminOfficer extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminOfficer Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return OfficerModels::has('user')->orderBy('name')->get();
    }

    public function onSave()
    {
        $mail      = new MailManager();
        $generator = new Generator();
        $rules     = [
            'email'     => 'required|email|unique:pulangpisau_user_users,email',
            'password'  => 'required|min:6',
        ];
        $messages       = [];
        $attributeNames = [
            'email'     => 'email',
            'password'  => 'sandi',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        /**
         * Insert Into User
        */
        $user           = new UserModels;
        $user->email    = post('email');
        $user->code     = $generator->makeOfficerCode();
        $user->password = Hash::make(post('password'));
        $user->save();

        /**
         * Insert Into Officer
        */
        $officer          = new OfficerModels;
        $officer->user_id = $user->id;
        $officer->save();

        /**
         * Insert Into Role
        */
        $role = UserUserGroupModels::firstOrCreate([
            'user_id'  => $user->id,
            'group_id' => 2
        ]);

        $mail->officerWelcome($user);

        Flash::success('Petugas berhasil disimpan');
        return Redirect::refresh();
    }
}
