<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Counter\Models\Counter as CounterModels;

class AdminCounter extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminCounter Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return CounterModels::orderBy('created_at', 'desc')->get();
    }

    public function onSave()
    {
        $rules = [
            'name'        => 'required',
            'code'        => 'required|unique:pulangpisau_counter_counters,code',
            'description' => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'code'        => 'kode',
            'description' => 'deskripsi',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $counter              = new CounterModels;
        $counter->name        = post('name');
        $counter->code        = post('code');
        $counter->description = post('description');
        $counter->save();

        Flash::success('Loket berhasil disimpan');
        return Redirect::refresh();
    }

    public function onMassiveAction()
    {
        $param = post('parameter');
        var_dump($param);
        return;
        if(post('type') == 'show') {
            CounterModels::whereIn('parameter', $param)->update(['is_active' => 1]);
            Flash::success('Berhasil disimpan');
            return Redirect::refresh();
        }
    }
}
