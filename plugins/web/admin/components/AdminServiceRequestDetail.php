<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

class AdminServiceRequestDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminServiceRequestDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
