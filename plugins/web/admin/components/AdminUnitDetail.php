<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Unit\Models\Unit as UnitModels;

class AdminUnitDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminUnitDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $unit               = $this->getCurrent();

        $this->page['unit'] = $unit;
    }

    public function getCurrent()
    {
        return UnitModels::whereParameter($this->property('parameter'))->first();
    }



    /**
     * Action
     */
    public function onSave()
    {
        $rules = [
            'name'        => 'required',
            'code'        => 'required|unique:pulangpisau_unit_units,code,'.$this->getCurrent()->id,
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'code'        => 'kode',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $counter              = $this->getCurrent();
        $counter->name        = post('name');
        $counter->code        = post('code');
        $counter->description = post('description');
        $counter->save();

        Flash::success('Unit petugas berhasil disimpan');
        return Redirect::refresh();
    }
}
