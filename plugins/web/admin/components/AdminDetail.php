<?php namespace Web\Admin\Components;

use Flash;
use Redirect;

use Pulangpisau\Core\Classes\SessionManager;

use Cms\Classes\ComponentBase;

class AdminDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = $this->getUser();
        if($user) {
            $this->page['userLogin'] = $user;
        }
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }
}
