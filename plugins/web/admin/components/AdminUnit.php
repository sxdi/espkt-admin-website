<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Unit\Models\Unit as UnitModels;

class AdminUnit extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminUnit Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return UnitModels::orderBy('name')->get();
    }

    public function onSave()
    {
        $rules = [
            'name'        => 'required',
            'code'        => 'required|unique:pulangpisau_unit_units,code',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'code'        => 'kode',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $counter              = new UnitModels;
        $counter->name        = post('name');
        $counter->code        = post('code');
        $counter->description = post('description');
        $counter->save();

        Flash::success('Unit petugas berhasil disimpan');
        return Redirect::refresh();
    }
}
