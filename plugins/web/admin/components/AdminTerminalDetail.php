<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Counter\Models\Counter as CounterModels;
use Pulangpisau\Counter\Models\Terminal as TerminalModels;

class AdminTerminalDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminTerminalDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $terminal               = $this->getCurrent();


        $this->page['terminal'] = $terminal;
    }

    public function getCurrent()
    {
        return TerminalModels::whereParameter($this->property('parameter'))->first();
    }


    /**
     * Action
    */
    public function onSave()
    {
        $rules = [
            'counter_id' => 'required',
            'number'     => 'required|numeric|min:1|max:10|unique:pulangpisau_counter_terminals,number,'.$this->getCurrent()->id,
        ];
        $messages       = [];
        $attributeNames = [
            'counter_id' => 'loket',
            'number'     => 'nomor',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $terminal             = $this->getCurrent();
        $terminal->counter_id = post('counter_id');
        $terminal->number     = post('number');
        $terminal->save();

        Flash::success('Terminal berhasil disimpan');
        return Redirect::refresh();
    }
}
