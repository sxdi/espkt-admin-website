<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Pulangpisau\Request\Models\RequestFeedback as RequestFeedbackModels;

class AdminFeedback extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminFeedback Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return RequestFeedbackModels::orderBy('created_at', 'desc')->get();
    }
}
