<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

class AdminQueueDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminQueueDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
