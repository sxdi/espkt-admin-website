<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\SessionManager;

use Pulangpisau\User\Models\User as UserModels;
use Pulangpisau\Officer\Models\Officer as OfficerModels;

class AdminProfile extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminProfile Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function onSave()
    {
        $user  = $this->getUser();
        $rules = [
            'name'        => 'required',
            'dob'         => 'required|date',
            'gender'      => 'required|in:male,female',
            'email'       => 'required|email|unique:pulangpisau_user_users,email,'.$user->id,
            'phone'       => 'required',
            'province_id' => 'required',
            'regency_id'  => 'required',
            'district_id' => 'required',
            'village_id'  => 'required',
            'address'     => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'dob'         => 'tanggal lahir',
            'gender'      => 'jenis kelamin',
            'email'       => 'emai',
            'phone'       => 'no telepon',
            'province_id' => 'provinsi',
            'regency_id'  => 'kota/kabupaten',
            'district_id' => 'kecamatan',
            'village_id'  => 'kelurahan',
            'address'     => 'alamat lengkap',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $u = UserModels::whereId($user->id)->update([
            'email' => post('email')
        ]);

        $officer = OfficerModels::whereUserId($user->id)->update([
            'name'        => post('name'),
            'dob'         => post('dob'),
            'gender'      => post('gender'),
            'phone'       => post('phone'),
            'province_id' => post('province_id'),
            'regency_id'  => post('regency_id'),
            'district_id' => post('district_id'),
            'village_id'  => post('village_id'),
            'address'     => post('address')
        ]);

        Flash::success('Profile berhasil disimpan');
        return Redirect::refresh();
    }
}
