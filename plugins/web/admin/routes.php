<?php


Route::group(['middleware' => ['Web\Admin\Middleware\IsNotLogin']], function () {
    /**
     * Masuk
    */
    Route::get('/', function ()    {
        return App::make('Cms\Classes\Controller')->run('/');
    });
});

Route::group(['middleware' => ['Web\Admin\Middleware\IsLogin']], function () {
    /**
     * Dashboard
    */
    Route::get('dashboard', function ()    {
        return App::make('Cms\Classes\Controller')->run('dashboard');
    });

    /**
     * Permohonan
    */
    Route::group(['prefix' => 'permohonan'], function () {
        Route::get('/', function ()    {
            return App::make('Cms\Classes\Controller')->run('permohonan');
        });

        Route::get('detail', function ($parameter)    {
            return App::make('Cms\Classes\Controller')->run('permohonan/detail/'.$parameter);
        })->where('parameter', '(.*)?');
    });

    /**
     * Penilaian
    */
    Route::group(['prefix' => 'penilaian', 'middleware' => 'Web\Admin\Middleware\HasRole:administrator|staff'], function () {
        Route::get('/', function ()    {
            return App::make('Cms\Classes\Controller')->run('penilaian');
        });

        Route::get('detail', function ($parameter)    {
            return App::make('Cms\Classes\Controller')->run('penilaian/detail/'.$parameter);
        })->where('parameter', '(.*)?');
    });

    /**
     * Layanan
    */
    Route::group(['prefix' => 'layanan'], function () {
        Route::get('/', function ()    {
            return App::make('Cms\Classes\Controller')->run('layanan');
        })->middleware('Web\Admin\Middleware\HasRole:administrator|staff');

        Route::get('buat', function ()    {
            return App::make('Cms\Classes\Controller')->run('layanan/buat');
        })->middleware('Web\Admin\Middleware\HasRole:administrator');

        Route::get('ubah', function ($parameter)    {
            return App::make('Cms\Classes\Controller')->run('layanan/ubah/'.$parameter);
        })->where('parameter', '(.*)?')->middleware('Web\Admin\Middleware\HasRole:administrator');
    });

    /**
     * Sektor
    */
    Route::group(['prefix' => 'sektor'], function () {
        Route::get('/', function ()    {
            return App::make('Cms\Classes\Controller')->run('sektor');
        })->middleware('Web\Admin\Middleware\HasRole:administrator|staff');

        Route::get('tambah', function ()    {
            return App::make('Cms\Classes\Controller')->run('sektor/tambah');
        })->middleware('Web\Admin\Middleware\HasRole:administrator');

        Route::get('ubah', function ($parameter)    {
            return App::make('Cms\Classes\Controller')->run('sektor/ubah/'.$parameter);
        })->where('parameter', '(.*)?')->middleware('Web\Admin\Middleware\HasRole:administrator');
    });

    /**
     * Loket
    */
    Route::group(['prefix' => 'loket'], function () {
        Route::get('/', function ()    {
            return App::make('Cms\Classes\Controller')->run('loket');
        })->middleware('Web\Admin\Middleware\HasRole:administrator|staff');

        Route::get('tambah', function ()    {
            return App::make('Cms\Classes\Controller')->run('loket/tambah');
        })->middleware('Web\Admin\Middleware\HasRole:administrator');

        Route::get('ubah', function ($parameter)    {
            return App::make('Cms\Classes\Controller')->run('loket/ubah/'.$parameter);
        })->where('parameter', '(.*)?')->middleware('Web\Admin\Middleware\HasRole:administrator');
    });

    /**
     * Terminal
    */
    Route::group(['prefix' => 'terminal'], function () {
        Route::get('/', function ()    {
            return App::make('Cms\Classes\Controller')->run('terminal');
        })->middleware('Web\Admin\Middleware\HasRole:administrator|staff');

        Route::get('tambah', function ()    {
            return App::make('Cms\Classes\Controller')->run('terminal/tambah');
        })->middleware('Web\Admin\Middleware\HasRole:administrator');

        Route::get('ubah', function ($parameter)    {
            return App::make('Cms\Classes\Controller')->run('terminal/ubah/'.$parameter);
        })->where('parameter', '(.*)?')->middleware('Web\Admin\Middleware\HasRole:administrator');
    });

    /**
     * Petugas
    */
    Route::group(['prefix' => 'petugas'], function () {
        Route::get('/', function ()    {
            return App::make('Cms\Classes\Controller')->run('petugas');
        })->middleware('Web\Admin\Middleware\HasRole:administrator|staff');

        Route::get('tambah', function ()    {
            return App::make('Cms\Classes\Controller')->run('petugas/tambah');
        })->middleware('Web\Admin\Middleware\HasRole:administrator');

        Route::get('ubah', function ($parameter)    {
            return App::make('Cms\Classes\Controller')->run('petugas/ubah/'.$parameter);
        })->where('parameter', '(.*)?')->middleware('Web\Admin\Middleware\HasRole:administrator');


        /**
         * Petugas Bagian
        */
        Route::group(['prefix' => 'bagian'], function () {
            Route::get('/', function ()    {
                return App::make('Cms\Classes\Controller')->run('petugas/bagian');
            })->middleware('Web\Admin\Middleware\HasRole:administrator|staff');

            Route::get('tambah', function ()    {
                return App::make('Cms\Classes\Controller')->run('petugas/bagian/tambah');
            })->middleware('Web\Admin\Middleware\HasRole:administrator');

            Route::get('ubah', function ($parameter)    {
                return App::make('Cms\Classes\Controller')->run('petugas/bagian/ubah/'.$parameter);
            })->where('parameter', '(.*)?')->middleware('Web\Admin\Middleware\HasRole:administrator');
        });

        /**
         * Petugas Unit
        */
        Route::group(['prefix' => 'unit'], function () {
            Route::get('/', function ()    {
                return App::make('Cms\Classes\Controller')->run('petugas/unit');
            })->middleware('Web\Admin\Middleware\HasRole:administrator|staff');

            Route::get('tambah', function ()    {
                return App::make('Cms\Classes\Controller')->run('petugas/unit/tambah');
            })->middleware('Web\Admin\Middleware\HasRole:administrator');

            Route::get('ubah', function ($parameter)    {
                return App::make('Cms\Classes\Controller')->run('petugas/unit/ubah/'.$parameter);
            })->where('parameter', '(.*)?')->middleware('Web\Admin\Middleware\HasRole:administrator');
        });
    });

    /**
     * Antrian
    */
    Route::group(['prefix' => 'antrian'], function () {
        Route::get('/', function ()    {
            return App::make('Cms\Classes\Controller')->run('antrian');
        });

        /**
         * Antrian Loket
        */
        Route::group(['prefix' => 'loket'], function () {
            Route::get('/', function ()    {
                return App::make('Cms\Classes\Controller')->run('antrian/loket');
            });
        });

         /**
         * Antrian Terminal
        */
        Route::group(['prefix' => 'terminal'], function () {
            Route::get('/', function ($parameter)    {
                return App::make('Cms\Classes\Controller')->run('terminal/'.$parameter)->where('parameter', '(.*)?');
            });
        });
    });

    /**
     * User
    */
    Route::group(['prefix' => 'u'], function () {
        Route::get('profile', function ()    {
            return App::make('Cms\Classes\Controller')->run('u/profile');
        });

        Route::get('password', function ()    {
            return App::make('Cms\Classes\Controller')->run('u/password');
        });
    });
});
