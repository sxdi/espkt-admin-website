<?php namespace Web\Auth\Components;

use Hash;
use Flash;
use Redirect;
use Validator;

use Pulangpisau\Core\Classes\SessionManager;

use Cms\Classes\ComponentBase;

use Pulangpisau\User\Models\User as UserModels;

class Auth extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Auth Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onLogin()
    {
        $rules = [
            'id'       => 'required',
            'password' => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'id'       => 'identitas',
            'password' => 'sandi',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $user = UserModels::whereCode(post('id'))->first();
        if(!$user) {
            Flash::error('Data tidak ditemkan');
            return;
        }

        $check = Hash::check(post('password'), $user->password);
        if(!$check) {
            Flash::error('Sandi tidak sesuai');
            return;
        }

        $session = new SessionManager();
        $session->set($user);
        Flash::success('Selamat datang '.$user->email);

        if(input('callback')) {
            return Redirect::to(input('callback'));
        }

        return Redirect::to('/dashboard');
    }

    public function onLogout()
    {
        $session = new SessionManager();
        $user = $session->get();

        if($user) {
            $session->destroy();
            Flash::success('Berhasil keluar');
            return Redirect::to('/');
        }
    }
}
