<?php namespace Web\Location\Components;

use Cms\Classes\ComponentBase;

use Pulangpisau\Location\Models\Province;
use Pulangpisau\Location\Models\Regency;
use Pulangpisau\Location\Models\District;
use Pulangpisau\Location\Models\Village;

class Location extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Location Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getProvince()
    {
        return Province::orderBy('name', 'asc')->get();
    }

    public function getRegencyById($provinceId)
    {
        return Regency::orderBy('name', 'asc')->whereProvinceId($provinceId)->get();
    }

    public function getDistrictById($regencyId)
    {
        return District::orderBy('name', 'asc')->whereRegencyId($regencyId)->get();
    }

    public function getVillageById($districtId)
    {
        return Village::orderBy('name', 'asc')->whereDistrictId($districtId)->get();
    }

    public function onGetRegency()
    {
        $regency                 = Regency::orderBy('name', 'asc')->whereProvinceId(post('province_id'))->get();
        $this->page['regencies'] = $regency;
    }

    public function onGetDistrict()
    {
        $district                = District::orderBy('name', 'asc')->whereRegencyId(post('regency_id'))->get();
        $this->page['districts'] = $district;
    }

    public function onGetVillage()
    {
        $village                = Village::orderBy('name', 'asc')->whereDistrictId(post('district_id'))->get();
        $this->page['villages'] = $village;
    }
}
