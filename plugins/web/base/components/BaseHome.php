<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

use Pulangpisau\Counter\Models\Counter as CounterModels;
use Pulangpisau\Queue\Models\Queue as QueueModels;

class BaseHome extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseHome Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['counters'] = $this->getCounter();;
    }

    public function getCounter()
    {
        return CounterModels::orderBy('code')->get();
    }

    public function getQueueByCounter($id)
    {
        return QueueModels::whereCounterId($id)->whereStatus('finish')->whereDate('created_at', 'like', date('Y-m-d'))->first();
    }

    public function onCall()
    {
        $this->page['channel'] = post('channel');

        if(post('number') >= 10) {
            $this->page['value1'] = substr(post('number'), 0, 1);
            $this->page['value2'] = substr(post('number'), 1, 2);
        }

        $this->page['number'] = post('number');
    }
}
