<?php namespace Pulangpisau\Counter;

use Backend;
use System\Classes\PluginBase;

/**
 * Counter Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Counter',
            'description' => 'No description provided yet...',
            'author'      => 'Pulangpisau',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Pulangpisau\Counter\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'pulangpisau.counter.some_permission' => [
                'tab' => 'Counter',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'counter' => [
                'label'       => 'Counter',
                'url'         => Backend::url('pulangpisau/counter/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['pulangpisau.counter.*'],
                'order'       => 500,
            ],
        ];
    }
}
