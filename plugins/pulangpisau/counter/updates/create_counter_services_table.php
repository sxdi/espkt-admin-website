<?php namespace Pulangpisau\Counter\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCounterServicesTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_counter_counter_services', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('counter_id');
            $table->integer('service_id');
            $table->integer('option_id')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_counter_counter_services');
    }
}
