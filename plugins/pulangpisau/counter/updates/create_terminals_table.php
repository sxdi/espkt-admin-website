<?php namespace Pulangpisau\Counter\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTerminalsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_counter_terminals', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('counter_id');
            $table->integer('number');
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_counter_terminals');
    }
}
