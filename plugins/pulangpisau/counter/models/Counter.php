<?php namespace Pulangpisau\Counter\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * Counter Model
 */
class Counter extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_counter_counters';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'terminals' => [
            'Pulangpisau\Counter\Models\Terminal',
            'key'      => 'id',
            'otherKey' => 'counter_id'
        ],
    ];
    public $belongsTo     = [];
    public $belongsToMany = [
        'services' => [
            'Pulangpisau\Service\Models\Service',
            'table'   => 'pulangpisau_counter_counter_services',
            'key'      => 'counter_id',
            'otherKey' => 'service_id'
        ]
    ];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
