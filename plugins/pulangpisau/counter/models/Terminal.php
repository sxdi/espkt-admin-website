<?php namespace Pulangpisau\Counter\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * Terminal Model
 */
class Terminal extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_counter_terminals';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'counter' => [
            'Pulangpisau\Counter\Models\Counter',
            'key'      => 'counter_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
