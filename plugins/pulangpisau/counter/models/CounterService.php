<?php namespace Pulangpisau\Counter\Models;

use Model;

/**
 * CounterService Model
 */
class CounterService extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_counter_counter_services';

    /**
     * @var string The database table used by the model.
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'counter' => [
            'Pulangpisau\Counter\Models\Counter',
            'id'       => 'counter_id',
            'otherKey' => 'id'
        ],
        'service' => [
            'Pulangpisau\Service\Models\Service',
            'id'       => 'service_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
