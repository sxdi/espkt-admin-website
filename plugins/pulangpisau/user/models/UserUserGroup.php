<?php namespace Pulangpisau\User\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * UserUserGroup Model
 */
class UserUserGroup extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_user_user_user_groups';

    /**
     * @var array Guarded fields
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['user_id', 'group_id'];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'role' => [
            'Pulangpisau\User\Models\UserGroup',
            'key'      => 'group_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
