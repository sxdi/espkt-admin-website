<?php namespace Pulangpisau\User\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * User Model
 */
class User extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_user_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'roles' => [
            'Pulangpisau\User\Models\UserUserGroup',
            'key'      => 'user_id',
            'otherKey' => 'id'
        ],
        'services' => [
            'Pulangpisau\User\Models\UserService',
            'key'      => 'user_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsTo     = [
        'officer' => [
            'Pulangpisau\Officer\Models\Officer',
            'key'      => 'id',
            'otherKey' => 'user_id',
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'avatar' => ['System\Models\File']
    ];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator        = new Generator();
        $this->parameter = $generator->make();
    }

    public function hasAccess($accessSlug)
    {
        $roles  = $this->roles;
        $access = [];
        foreach ($roles as $role) {
            array_push($access, $role->role->slug);
        }

        if(is_array($accessSlug)) {
            return count(array_intersect($accessSlug, $access));
        }

        return in_array($accessSlug, $access);
    }
}
