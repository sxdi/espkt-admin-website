<?php namespace Pulangpisau\User\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * UserGroup Model
 */
class UserGroup extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_user_user_groups';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $genrator        = new Generator();
        $this->parameter = $generator->make();
    }

    public function beforeSave()
    {
        $this->slug = str_slug($this->name);
    }
}
