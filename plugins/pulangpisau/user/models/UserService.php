<?php namespace Pulangpisau\User\Models;

use Model;

/**
 * UserService Model
 */
class UserService extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_user_user_services';

    /**
     * @var string The database table used by the model.
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'service' => [
            'Pulangpisau\Service\Models\Service',
            'key'      => 'service_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
