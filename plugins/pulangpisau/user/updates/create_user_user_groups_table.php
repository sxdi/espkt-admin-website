<?php namespace Pulangpisau\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUserUserGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_user_user_user_groups', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('user_id');
            $table->string('group_id');
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_user_user_user_groups');
    }
}
