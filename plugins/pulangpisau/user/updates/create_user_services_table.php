<?php namespace Pulangpisau\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUserServicesTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_user_user_services', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id');
            $table->integer('service_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_user_user_services');
    }
}
