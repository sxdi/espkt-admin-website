<?php namespace Pulangpisau\Officer\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * Officer Model
 */
class Officer extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_officer_officers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'user' => [
            'Pulangpisau\User\Models\User',
            'key'      => 'user_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
