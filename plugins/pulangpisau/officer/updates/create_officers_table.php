<?php namespace Pulangpisau\Officer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOfficersTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_officer_officers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->enum('gender', ['male', 'female']);
            $table->date('dob');
            $table->string('phone');
            $table->string('province_id');
            $table->string('regency_id');
            $table->string('district_id');
            $table->string('village_id');
            $table->string('address');
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_officer_officers');
    }
}
