<?php namespace Pulangpisau\Officer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOfficerUnitsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_officer_officer_units', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('officer_id');
            $table->integer('unit_id');
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_officer_officer_units');
    }
}
