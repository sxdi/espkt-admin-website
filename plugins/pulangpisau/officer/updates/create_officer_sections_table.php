<?php namespace Pulangpisau\Officer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOfficerSectionsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_officer_officer_sections', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('officer_id');
            $table->integer('section_id');
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_officer_officer_sections');
    }
}
