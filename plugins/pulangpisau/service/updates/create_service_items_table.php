<?php namespace Pulangpisau\Service\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServiceItemsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_service_service_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('service_id');
            $table->string('name');
            $table->string('code');
            $table->string('video_url')->nullable();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_service_service_items');
    }
}
