<?php namespace Pulangpisau\Service\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServicesTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_service_services', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->string('name');
            $table->string('day_open')->nullable();
            $table->string('time_open')->nullable();
            $table->string('code'->nullable();
            $table->string('model')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->boolean('is_active')->nullable()->default(1);
            $table->string('slug');
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_service_services');
    }
}
