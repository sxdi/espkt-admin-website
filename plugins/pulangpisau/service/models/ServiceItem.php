<?php namespace Pulangpisau\Service\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * ServiceItem Model
 */
class ServiceItem extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_service_service_items';

    /**
     * @var string The database table used by the model.
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'service' => [
            'Pulangpisau\Service\Models\Service',
            'key'      => 'service_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'pdf'   => 'System\Models\File',
        'video' => 'System\Models\File'
    ];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
