<?php namespace Pulangpisau\Service\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * Service Model
 */
class Service extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_service_services';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'items' => [
            'Pulangpisau\Service\Models\ServiceItem',
            'key'      => 'id',
            'otherKey' => 'service_id'
        ],
        'childs' => [
            'Pulangpisau\Service\Models\Service',
            'key'      => 'parent_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsTo     = [
        'counter' => [
            'Pulangpisau\Counter\Models\CounterService',
            'key'      => 'id',
            'otherKey' => 'service_id'
        ],
        'parent' => [
            'Pulangpisau\Service\Models\Service',
            'key'      => 'parent_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [
        'counters' => [
            'Pulangpisau\Counter\Models\Counter',
            'table'   => 'pulangpisau_counter_counter_services',
            'key'      => 'service_id',
            'otherKey' => 'counter_id'
        ],
    ];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'picture'  => ['System\Models\File'],
        'formulir' => ['System\Models\File'],
    ];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }

    public function afterCreate()
    {
        $this->slug = str_slug($this->name);
    }
}
