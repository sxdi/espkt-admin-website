<?php namespace Pulangpisau\Service;

use Backend;
use System\Classes\PluginBase;

/**
 * Service Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Service',
            'description' => 'No description provided yet...',
            'author'      => 'Pulangpisau',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Pulangpisau\Service\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'pulangpisau.service.some_permission' => [
                'tab' => 'Service',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'service' => [
                'label'       => 'Service',
                'url'         => Backend::url('pulangpisau/service/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['pulangpisau.service.*'],
                'order'       => 500,
            ],
        ];
    }
}
