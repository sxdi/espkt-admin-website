<?php namespace Pulangpisau\Unit;

use Backend;
use System\Classes\PluginBase;

/**
 * Unit Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Unit',
            'description' => 'No description provided yet...',
            'author'      => 'Pulangpisau',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Pulangpisau\Unit\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'pulangpisau.unit.some_permission' => [
                'tab' => 'Unit',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'unit' => [
                'label'       => 'Unit',
                'url'         => Backend::url('pulangpisau/unit/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['pulangpisau.unit.*'],
                'order'       => 500,
            ],
        ];
    }
}
