<?php namespace Pulangpisau\Queue\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * Queue Model
 */
class Queue extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_queue_queues';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['counter_id', 'number'];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'counter' => [
            'Pulangpisau\Counter\Models\Counter',
            'key'      => 'counter_id',
            'otherKey' => 'id'
        ],
        'terminal' => [
            'Pulangpisau\Counter\Models\Terminal',
            'key'      => 'terminal_id',
            'otherKey' => 'id'
        ],
        'user' => [
            'Pulangpisau\User\Models\User',
            'key'      => 'user_id',
            'otherKey' => 'id'
        ],
        'queueRequest' => [
            'Pulangpisau\Request\Models\RequestQueue',
            'key'      => 'id',
            'otherKey' => 'queue_id'
        ],
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
