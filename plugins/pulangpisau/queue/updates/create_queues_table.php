<?php namespace Pulangpisau\Queue\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateQueuesTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_queue_queues', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('counter_id');
            $table->integer('terminal_id');
            $table->integer('number');
            $table->timestamps();
            $table->enum('status', ['waiting', 'handle', 'finish'])->nullable()->default('waiting');
            $table->boolean('is_online')->nullable()->default(0);
            $table->boolean('is_coming')->nullable()->default(0);
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_queue_queues');
    }
}
