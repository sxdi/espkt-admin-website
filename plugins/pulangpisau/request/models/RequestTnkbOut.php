<?php namespace Pulangpisau\Request\Models;

use Model;

/**
 * RequestTnkbOut Model
 */
class RequestTnkbOut extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_request_request_tnkb_outs';

    /**
     * @var string The database table used by the model.
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['hasKtp', 'hasPurchase', 'hasStnk', 'hasStnkCopy', 'hasBpkb', 'hasBpkbCopy', 'hasPhysical'];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
