<?php namespace Pulangpisau\Request\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * Request Model
 */
class Request extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_request_requests';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'code'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'items' => [
            'Pulangpisau\Request\Models\RequestItem',
            'key'      => 'request_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->code      = $generator->makeNumber();
        $this->parameter = $generator->make();
    }
}
