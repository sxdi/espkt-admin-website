<?php namespace Pulangpisau\Request\Models;

use Model;

/**
 * RequestHearseOther Model
 */
class RequestHearseOther extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_request_request_hearse_others';

    /**
     * @var string The database table used by the model.
    */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
