<?php namespace Pulangpisau\Request\Models;

use Model;

/**
 * RequestPickSick Model
 */
class RequestPickSick extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_request_request_pick_sicks';

    /**
     * @var string The database table used by the model.
    */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
    */
    public $dates = ['scheduled_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'origin_regency' => [
            'Pulangpisau\Location\Models\Regency',
            'key'      => 'origin_regency_id',
            'otherKey' => 'id'
        ],
        'origin_district' => [
            'Pulangpisau\Location\Models\District',
            'key'      => 'origin_district_id',
            'otherKey' => 'id'
        ],
        'origin_village' => [
            'Pulangpisau\Location\Models\Village',
            'key'      => 'origin_village_id',
            'otherKey' => 'id'
        ],

        'destination_regency' => [
            'Pulangpisau\Location\Models\Regency',
            'key'      => 'destination_regency_id',
            'otherKey' => 'id'
        ],
        'destination_district' => [
            'Pulangpisau\Location\Models\District',
            'key'      => 'destination_district_id',
            'otherKey' => 'id'
        ],
        'destination_village' => [
            'Pulangpisau\Location\Models\Village',
            'key'      => 'destination_village_id',
            'otherKey' => 'id'
        ],
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
