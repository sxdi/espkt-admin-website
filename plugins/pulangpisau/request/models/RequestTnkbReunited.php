<?php namespace Pulangpisau\Request\Models;

use Model;

/**
 * RequestTnkbReunited Model
 */
class RequestTnkbReunited extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_request_request_tnkb_reuniteds';

    /**
     * @var string The database table used by the model.
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['hasKtp', 'hasKtpCopy', 'hasStnk', 'hasBpkb', 'hasBpkbCopy', 'hasKtpNew', 'hasPurchase', 'hasPhysical'];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
