<?php namespace Pulangpisau\Request\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * RequestOfficer Model
 */
class RequestOfficer extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_request_request_officers';

    /**
     * @var array Guarded fields
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['item_id', 'officer_id'];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
