<?php namespace Pulangpisau\Request\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * RequestItem Model
 */
class RequestItem extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_request_request_items';

    /**
     * @var string The database table used by the model.
     */
    public $dates = ['scheduled_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'parent_id'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        // 'officers' => [
        //     'Pulangpisau\Request\Models\RequestOfficer',
        //     'key'      => 'item_id',
        // ],
    ];
    public $belongsTo     = [
        'request' => [
            'Pulangpisau\Request\Models\Request',
            'key'      => 'request_id',
            'otherKey' => 'id'
        ],
        'service' => [
            'Pulangpisau\Service\Models\Service',
            'key'      => 'service_id',
            'otherKey' => 'id'
        ],
        'user' => [
            'Pulangpisau\User\Models\User',
            'key'      => 'user_id',
            'otherKey' => 'id'
        ],

        'feedback' => [
            'Pulangpisau\Request\Models\RequestFeedback',
            'key'      => 'id',
            'otherKey' => 'item_id'
        ],
        'queue' => [
            'Pulangpisau\Request\Models\RequestQueue',
            'key'      => 'id',
            'otherKey' => 'item_id'
        ],
        'customer' => [
            'Pulangpisau\Request\Models\RequestCustomer',
            'key'      => 'id',
            'otherKey' => 'item_id'
        ],
        'counter' => [
            'Pulangpisau\Counter\Models\CounterService',
            'key'      => 'service_id',
            'otherKey' => 'service_id'
        ],
    ];
    public $belongsToMany = [];
    public $morphTo       = [
        'requestable' => []
    ];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
