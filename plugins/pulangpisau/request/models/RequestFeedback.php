<?php namespace Pulangpisau\Request\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * RequestFeedback Model
 */
class RequestFeedback extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_request_request_feedbacks';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'item_id'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'item' => [
            'Pulangpisau\Request\Models\RequestItem',
            'key'      => 'item_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
