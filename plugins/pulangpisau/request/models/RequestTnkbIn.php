<?php namespace Pulangpisau\Request\Models;

use Model;

/**
 * RequestTnkbIn Model
 */
class RequestTnkbIn extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_request_request_tnkb_ins';

    /**
     * @var string The database table used by the model.
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['hasPurchase', 'hasKtp', 'hasKtpCopy', 'hasStnk', 'hasStnkCopy', 'hasBpkb', 'hasBpkbCopy', 'hasPhysical', 'hasMoveLetter'];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
