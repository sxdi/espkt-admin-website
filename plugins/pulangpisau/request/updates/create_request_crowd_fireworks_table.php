<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestCrowdFireworksTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_crowd_fireworks', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->enum('hasPermitPlace', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasPermitPolice', ['true', 'false'])->nullable()->default('true');
            $table->string('purpose')->nullable();
            $table->string('qty')->nullable();
            $table->string('source')->nullable();
            $table->string('kind')->nullable();
            $table->string('date')->nullable();
            $table->string('time')->nullable();
            $table->string('pic')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_crowd_fireworks');
    }
}
