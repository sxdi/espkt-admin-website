<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestFeedbacksTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_feedbacks', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->boolean('value')->nullable()->default(0);
            $table->text('comment')->nullable();
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_feedbacks');
    }
}
