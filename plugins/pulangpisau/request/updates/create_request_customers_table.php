<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestCustomersTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_customers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('customer_id');
            $table->string('name');
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->string('job');
            $table->string('nationality');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_customers');
    }
}
