<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestItemsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('request_id');
            $table->integer('service_id');
            $table->string('requestable_type');
            $table->integer('requestable_id');
            $table->string('service_name');
            $table->date('scheduled_at')->nullable();
            $table->enum('kind', ['walkin', 'online'])->nullable()->default('walkin');
            $table->enum('channel', ['onsite', 'online'])->nullable()->default('onsite');
            $table->integer('user_id');
            $table->timestamps();
            $table->enum('status', ['request', 'progress', 'waiting', 'cancel', 'reject', 'finish', 'done']);
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_items');
    }
}
