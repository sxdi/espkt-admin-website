<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestCountersTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_counters', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('counter_id');
            $table->string('counter_name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_counters');
    }
}
