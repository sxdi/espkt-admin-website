<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestCrowdCampaignsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_crowd_campaigns', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->enum('hasProposal', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasPermitRequest', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasPermitPlace', ['true', 'false'])->nullable()->default('true');
            $table->string('campaign_group')->nulable();
            $table->string('campaign_address')->nulable();
            $table->string('kind')->nulable();
            $table->string('date')->nulable();
            $table->string('time')->nulable();
            $table->string('place')->nulable();
            $table->string('participant')->nulable();
            $table->string('lead')->nulable();
            $table->string('vehicle')->nulable();
            $table->string('apk')->nulable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_crowd_campaigns');
    }
}
