<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestCrowdsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_crowds', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->enum('hasKtp', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasProposal', ['true', 'false'])->nullable()->default('true');
            $table->string('kind')->nullable();
            $table->string('place')->nullable();
            $table->string('participant')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_crowds');
    }
}
