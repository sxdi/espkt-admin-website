<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestLostsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_losts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->enum('kind', ['document', 'thing', 'etc'])->nullable();
            $table->text('content')->nullable();
            $table->string('date')->nullable();
            $table->string('time')->nullable();
            $table->string('place')->nullable();
            $table->text('description')->nullable();
            $table->enum('hasKtp', ['true', 'false'])->nullable()->default('true');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_losts');
    }
}
