<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestTnkbLostsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_tnkb_losts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->enum('hasKtp', ['true', 'false'])->nullable();
            $table->enum('hasKtpCopy', ['true', 'false'])->nullable();
            $table->enum('hasStnk', ['true', 'false'])->nullable();
            $table->enum('hasStnkCopy', ['true', 'false'])->nullable();
            $table->enum('hasBpbk', ['true', 'false'])->nullable();
            $table->enum('hasBpbkCopy', ['true', 'false'])->nullable();
            $table->enum('hasLost', ['true', 'false'])->nullable();
            $table->enum('hasPhysical', ['true', 'false'])->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_tnkb_losts');
    }
}
