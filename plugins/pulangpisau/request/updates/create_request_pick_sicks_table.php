<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestPickSicksTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_pick_sicks', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->datetime('scheduled_at')->nullable();
            $table->string('origin_regency_id')->nullable();
            $table->string('origin_village_id')->nullable();
            $table->string('origin_district_id')->nullable();
            $table->string('origin_address')->nullable();
            $table->string('origin_rt')->nullable();
            $table->string('origin_rw')->nullable();
            $table->string('destination_regency_id')->nullable();
            $table->string('destination_village_id')->nullable();
            $table->string('destination_district_id')->nullable();
            $table->string('destination_address')->nullable();
            $table->string('destination_rt')->nullable();
            $table->string('destination_rw')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_pick_sicks');
    }
}
