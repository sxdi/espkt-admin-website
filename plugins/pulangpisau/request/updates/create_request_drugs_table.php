<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestDrugsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_drugs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->string('header_doctor')->nullable();
            $table->string('purpose')->nullable();
            $table->enum('hasDrug', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasKtp', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasKk', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasUrine', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasPhoto', ['true', 'false'])->nullable()->default('true');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_drugs');
    }
}
