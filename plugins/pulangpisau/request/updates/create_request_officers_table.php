<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestOfficersTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_officers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('officer_id');
            $table->string('name');
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_officers');
    }
}
