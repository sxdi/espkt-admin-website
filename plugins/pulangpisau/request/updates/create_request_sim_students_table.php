<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestSimStudentsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_sim_students', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->string('type')->nullable();
            $table->enum('hasSim', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasKtp', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasKtpCopy', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasHealthy', ['true', 'false'])->nullable()->default('true');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_sim_students');
    }
}
