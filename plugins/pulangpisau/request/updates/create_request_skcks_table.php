<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestSkcksTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_skcks', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->enum('hasSkck', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasFinger', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasPhoto', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasKtp', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasKk', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasAkta', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasPaspor', ['true', 'false'])->nullable()->default('true');

            $table->enum('is_criminal', ['true', 'false'])->nullable();
            $table->text('criminal_case')->nullable();
            $table->text('criminal_punishment')->nullable();
            $table->text('criminal_punishment_process')->nullable();
            $table->text('criminal_punishment_description')->nullable();

            $table->enum('is_violation', ['true', 'false'])->nullable();
            $table->text('violation_case')->nullable();
            $table->text('vionlation_punishment')->nullable();

            $table->text('purpose')->nullable();
            $table->text('job_description')->nullable();
            $table->text('hobby')->nullable();
            $table->text('address')->nullable();

            $table->text('sponsor')->nullable();
            $table->text('sponsor_address')->nullable();
            $table->text('sponsor_phone')->nullable();
            $table->text('sponsor_business')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_skcks');
    }
}
