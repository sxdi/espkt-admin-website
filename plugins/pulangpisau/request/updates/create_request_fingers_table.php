<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestFingersTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_fingers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->enum('hasKtp', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasPhoto', ['true', 'false'])->nullable()->default('true');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_fingers');
    }
}
