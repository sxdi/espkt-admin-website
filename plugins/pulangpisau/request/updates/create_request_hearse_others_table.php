<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestHearseOthersTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_hearse_others', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->datetime('scheduled_at')->nullable();
            $table->string('type')->nullable();
            $table->text('origin_address')->nullable();
            $table->string('origin_rt')->nullable();
            $table->string('origin_rw')->nullable();
            $table->text('destination_address')->nullable();
            $table->string('destination_rt')->nullable();
            $table->string('destination_rw')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_hearse_others');
    }
}
