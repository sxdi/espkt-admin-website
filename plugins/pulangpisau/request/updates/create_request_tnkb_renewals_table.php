<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestTnkbRenewalsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_tnkb_renewals', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->enum('hasStnk', ['true', 'false'])->nullable();
            $table->enum('hasStnkCopy', ['true', 'false'])->nullable();
            $table->enum('hasBpkb', ['true', 'false'])->nullable();
            $table->enum('hasBpkbCopy', ['true', 'false'])->nullable();
            $table->enum('hasPhysical', ['true', 'false'])->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_tnkb_renewals');
    }
}
