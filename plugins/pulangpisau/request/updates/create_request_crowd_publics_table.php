<?php namespace Pulangpisau\Request\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestCrowdPublicsTable extends Migration
{
    public function up()
    {
        Schema::create('pulangpisau_request_request_crowd_publics', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->enum('hasProposal', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasPermitRequest', ['true', 'false'])->nullable()->default('true');
            $table->enum('hasPermitPlace', ['true', 'false'])->nullable()->default('true');
            $table->string('kind')->nullable();
            $table->string('date')->nullable();
            $table->string('time')->nullable();
            $table->string('place')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pulangpisau_request_request_crowd_publics');
    }
}
