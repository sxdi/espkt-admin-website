<?php namespace Pulangpisau\Core\Classes;

use Mail;
use Session;

use Pulangpisau\User\Models\User as UserModels;

class MailManager {

    public function officerWelcome($user)
    {
        $vars = [
            'user' => $user,
        ];

        Mail::send('pulangpisau.core::mail.user-welcome', $vars, function($message) use($user) {
            $message->to($user->email, $user->officer->name ? $user->officer->name : $user->email);
            $message->subject('Selamat datang di aplikasi satu pintu polres pulang pisau');
        });
    }
}
