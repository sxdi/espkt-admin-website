<?php namespace Pulangpisau\Core\Classes;

use Hash;

class Generator {

    public function make()
    {
        return md5(Hash::make(date('Y-m-d H:i:s')).$this->makeNumber());
    }

    public function makeNumber()
    {
        return rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
    }

    public function makeOfficerCode()
    {
        return rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
    }
}
