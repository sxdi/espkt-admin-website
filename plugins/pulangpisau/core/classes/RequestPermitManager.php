<?php namespace Pulangpisau\Core\Classes;


use Pulangpisau\Officer\Models\Officer as OfficerModels;

use Pulangpisau\Queue\Models\Queue as QueueModels;

use Pulangpisau\Request\Models\RequestOfficer as RequestOfficerModels;
use Pulangpisau\Request\Models\RequestCustomer as RequestCustomerModels;
use Pulangpisau\Request\Models\RequestQueue as RequestQueueModels;

class RequestPermitManager {

    public function getOfficer($id)
    {
        return OfficerModels::whereId($id)->first();
    }

    public function getQueue($ref)
    {
        return QueueModels::whereParameter($ref)->first()->id;
    }

    public function finishQueue($ref)
    {
        QueueModels::whereParameter($ref)->update([
            'status' => 'finish'
        ]);
    }

    public function makeQueue($itemId, $ref)
    {
        $this->finishQueue($ref);
        $queue = RequestQueueModels::firstOrCreate([
            'item_id'  => $itemId,
            'queue_id' => $this->getQueue($ref)
        ]);
        $queue->save();
    }

    public function makeRequester($itemId, $post)
    {
        $customer              = RequestCustomerModels::firstOrCreate([
            'item_id' => $itemId
        ]);
        $customer->name        = $post['name'];
        $customer->phone       = $post['phone'];
        $customer->email       = $post['email'];
        $customer->gender      = $post['gender'];
        $customer->job         = $post['job'];
        $customer->nationality = $post['nationality'];
        $customer->address     = $post['address'];
        $customer->save();
    }

    public function makeOfficer($itemId, $post)
    {
        if(count(post('user_id'))) {
            for ($i=0; $i < count($post['user_id']); $i++) {
                $officer = $this->getOfficer($post['user_id'][$i]);

                if($officer) {
                    $of      = RequestOfficerModels::firstOrCreate([
                        'item_id'    => $itemId,
                        'officer_id' => $officer->id
                    ]);
                    $of->name = $officer->name ? $officer->name : $officer->user->email;
                    $of->save();
                }
            }
        }
    }
}
