<?php namespace Pulangpisau\Core\Classes;

use Session;

use Pulangpisau\User\Models\User as UserModels;

class SessionManager {

    public function set($data)
    {
        return Session::set('userLogin', $data);
    }

    public function get()
    {
        $session = Session::get('userLogin');
        if($session) {
            $user    = UserModels::whereId($session->id)->first();
            return $user;
        }

        return false;
    }

    public function destroy()
    {
        Session::forget('userLogin');
        return;
    }
}
