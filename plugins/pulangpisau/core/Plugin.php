<?php namespace Pulangpisau\Core;

use Backend;
use System\Classes\PluginBase;

/**
 * Core Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Core',
            'description' => 'No description provided yet...',
            'author'      => 'Pulangpisau',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Pulangpisau\Core\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'pulangpisau.core.some_permission' => [
                'tab' => 'Core',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'core' => [
                'label'       => 'Core',
                'url'         => Backend::url('pulangpisau/core/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['pulangpisau.core.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'android' => [
                'label'       => 'Android',
                'description' => 'Manage available android applications.',
                'category'    => 'Site',
                'icon'        => 'icon-globe',
                'class'       => 'Pulangpisau\Setting\Models\Setting',
                'order'       => 100,
                'keywords'    => 'geography place placement'
            ]
        ];
    }


    /**
     * [registerMarkupTags description]
     * @return [type] [description]
     */
    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'ucwords' => [$this, 'makeUcWords'],
                'makePad' => [$this, 'makePad']
            ],
        ];
    }

    public function makeUcWords($text)
    {
        return ucwords(strtolower($text));
    }

    public function makePad($value)
    {
        return str_pad($value, 3, '0', STR_PAD_LEFT);
    }
}
