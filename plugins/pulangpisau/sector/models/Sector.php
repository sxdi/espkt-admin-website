<?php namespace Pulangpisau\Sector\Models;

use Model;
use Pulangpisau\Core\Classes\Generator;

/**
 * Sector Model
 */
class Sector extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pulangpisau_sector_sectors';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }

    public function beforeSave()
    {
        $this->slug = str_slug($this->name);
    }
}
