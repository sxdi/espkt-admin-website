<?php namespace Pulangpisau\Api\Controllers;

use Carbon\Carbon;
use Hash;
use Exception;
use Validator;

use Illuminate\Routing\Controller;

use Pulangpisau\Service\Models\Service as ServiceModels;

use Pulangpisau\Request\Models\Request as RequestModels;
use Pulangpisau\Request\Models\RequestItem as RequestItemModels;
use Pulangpisau\Request\Models\RequestCustomer as RequestCustomerModels;
use Pulangpisau\Request\Models\RequestQueue as RequestQueueModels;

use Pulangpisau\Request\Models\RequestSim as RequestSimModels;
use Pulangpisau\Request\Models\RequestSimStudent as RequestSimStudentModels;

use Pulangpisau\Request\Models\RequestSkck as RequestSkckModels;

use Pulangpisau\Request\Models\RequestTnkbYearly as RequestTnkbYearlyModels;
use Pulangpisau\Request\Models\RequestTnkbRenewal as RequestTnkbRenewalModels;
use Pulangpisau\Request\Models\RequestTnkbLost as RequestTnkbLostModels;
use Pulangpisau\Request\Models\RequestTnkbReunited as RequestTnkbReunitedModels;
use Pulangpisau\Request\Models\RequestTnkbOut as RequestTnkbOutModels;
use Pulangpisau\Request\Models\RequestTnkbIn as RequestTnkbInModels;

use Pulangpisau\Request\Models\RequestCrowd as RequestCrowdModels;
use Pulangpisau\Request\Models\RequestCrowdFirework as RequestCrowdFireworkModels;
use Pulangpisau\Request\Models\RequestCrowdPublic as RequestCrowdPublicModels;
use Pulangpisau\Request\Models\RequestCrowdCampaign as RequestCrowdCampaignModels;

use Pulangpisau\Request\Models\RequestHearse as RequestHearseModels;
use Pulangpisau\Request\Models\RequestHearseOther as RequestHearseOtherModels;

use Pulangpisau\Request\Models\RequestDrug as RequestDrugModels;

use Pulangpisau\Request\Models\RequestFinger as RequestFingerModels;

use Pulangpisau\Request\Models\RequestLost as RequestLostModels;

use Pulangpisau\Request\Models\RequestPickSick as RequestPickSickModels;

use Pulangpisau\Queue\Models\Queue as QueueModels;

use League\Fractal;

class Request extends Controller
{
    public function getService($code)
    {
        return ServiceModels::whereCode($code)->first();
    }

    public function getServiceRequest($id)
    {
        return ServiceModels::whereId($id)->first();
    }

    public function store()
    {
        $service        = $this->getService(post('service'));
        $serviceRequest = $this->getServiceRequest(post('requested'));
        $request        = new RequestModels;
        $request->save();

        $requestItem                     = new RequestItemModels;
        $requestItem->request_id         = $request->id;
        $requestItem->service_id         = post('requested');
        $requestItem->requestable_type   = $serviceRequest->model;
        $requestItem->requestable_id     = 0;
        $requestItem->service_name       = $serviceRequest->name;
        $requestItem->scheduled_at       = Carbon::parse(post('appointment'))->format('Y-m-d H:i:s');
        $requestItem->kind               = 'online';
        $requestItem->channel            = 'online';
        $requestItem->status             = 'waiting';
        $requestItem->save();

        $requestCustomer                 = new RequestCustomerModels;
        $requestCustomer->item_id        = $requestItem->id;
        $requestCustomer->customer_id    = 0;
        $requestCustomer->name           = post('name');
        $requestCustomer->place          = post('place');
        $requestCustomer->dob            = post('dob');
        $requestCustomer->phone          = post('phone');
        $requestCustomer->email          = post('email');
        $requestCustomer->gender         = post('gender');
        $requestCustomer->religion       = post('religion');
        $requestCustomer->marital_status = post('marital_status');
        $requestCustomer->job            = post('job');
        $requestCustomer->nationality    = post('nationality');
        $requestCustomer->number_id      = post('number_id');
        $requestCustomer->number_pasport = post('number_pasport');
        $requestCustomer->number_kitas   = post('number_kitas');
        $requestCustomer->address        = post('address');
        $requestCustomer->save();

        /**
         * Queue
        */
        $queueLast = QueueModels::whereDate('created_at', '=', date('Y-m-d'))->whereCounterId($serviceRequest->parent->counter->counter_id)->orderBy('id', 'desc')->first();

        $queue             = new QueueModels;
        $queue->counter_id = $serviceRequest->parent->counter->counter_id;
        $queue->number     = $queueLast ? $queueLast->number + 1 : 1;
        $queue->created_at = $requestItem->scheduled_at;
        $queue->status     = 'waiting';
        $queue->is_online  = 1;
        $queue->is_coming  = 0;
        $queue->save();

        $requestQueue = RequestQueueModels::firstOrCreate([
            'item_id'  => $requestItem->id,
            'queue_id' => $queue->id
        ]);
        $requestQueue->save();

        /**
         * Skck Table
         */
        if($service->code == 'permit-sim') {
            if(post('type') == 'sim-new') {
                $sim                    = new RequestSimModels;
                $sim->item_id           = $requestItem->id;
                $sim->type              = post('kind');
                $sim->hasGlasses        = post('hasGlasses');
                $sim->hasPsy            = post('hasPsy');
                $sim->hasCertified      = post('hasCertified');
                $sim->height            = post('height');
                $sim->education         = post('education');
                $sim->father            = post('father');
                $sim->mother            = post('mother');
                $sim->address_city      = post('address_city');
                $sim->address_village   = post('address_village');
                $sim->address_rt        = post('address_rt');
                $sim->address_rw        = post('address_rw');
                $sim->address_pos       = post('address_pos');
                $sim->emergency_city    = post('emergency_city');
                $sim->emergency_village = post('emergency_village');
                $sim->emergency_rt      = post('emergency_rt');
                $sim->emergency_rw      = post('emergency_rw');
                $sim->emergency_pos     = post('emergency_pos');
                $sim->save();
            }

            if(post('type') == 'sim-extend') {
                $sim                    = new RequestSimModels;
                $sim->item_id           = $requestItem->id;
                $sim->type              = post('kind');
                $sim->hasGlasses        = post('hasGlasses');
                $sim->hasPsy            = post('hasPsy');
                $sim->hasCertified      = post('hasCertified');
                $sim->height            = post('height');
                $sim->education         = post('education');
                $sim->father            = post('father');
                $sim->mother            = post('mother');
                $sim->address_city      = post('address_city');
                $sim->address_village   = post('address_village');
                $sim->address_rt        = post('address_rt');
                $sim->address_rw        = post('address_rw');
                $sim->address_pos       = post('address_pos');
                $sim->emergency_city    = post('emergency_city');
                $sim->emergency_village = post('emergency_village');
                $sim->emergency_rt      = post('emergency_rt');
                $sim->emergency_rw      = post('emergency_rw');
                $sim->emergency_pos     = post('emergency_pos');
                $sim->save();
            }

            if(post('type') == 'sim-student') {
                $sim                    = new RequestSimStudentModels;
                $sim->item_id           = $requestItem->id;
                $sim->type              = post('kind');
                $sim->hasGlasses        = post('hasGlasses');
                $sim->hasPsy            = post('hasPsy');
                $sim->hasCertified      = post('hasCertified');
                $sim->height            = post('height');
                $sim->education         = post('education');
                $sim->father            = post('father');
                $sim->mother            = post('mother');
                $sim->address_city      = post('address_city');
                $sim->address_village   = post('address_village');
                $sim->address_rt        = post('address_rt');
                $sim->address_rw        = post('address_rw');
                $sim->address_pos       = post('address_pos');
                $sim->emergency_city    = post('emergency_city');
                $sim->emergency_village = post('emergency_village');
                $sim->emergency_rt      = post('emergency_rt');
                $sim->emergency_rw      = post('emergency_rw');
                $sim->emergency_pos     = post('emergency_pos');
                $sim->save();
            }

            $requestItem->requestable_id = $sim->id;
            $requestItem->save();
        }

        /**
         * Skck Table
         */
        if($service->code == 'permit-skck') {
            $skck                                  = new RequestSkckModels;
            $skck->item_id                         = $requestItem->id;
            $skck->hasSkck                         = 'false';
            $skck->hasFinger                       = 'false';
            $skck->hasPaspor                       = post('hasPaspor');

            $skck->is_criminal                     = post('is_criminal');
            if(post('is_criminal')                 == 'true') {
                $skck->criminal_case                   = post('criminal_case');
                $skck->criminal_punishment             = post('criminal_punishment');
                $skck->criminal_punishment_process     = post('criminal_punishment_process');
                $skck->criminal_punishment_description = post('criminal_punishment_description');
            }

            $skck->is_violation                    = post('is_violation');
            if(post('is_violation')                == 'true') {
                $skck->violation_case                  = post('violation_case');
                $skck->vionlation_punishment           = post('vionlation_punishment');
            }

            if(post('is_sponsor')                  == 'true') {
                $skck->sponsor                         = post('sponsor');
                $skck->sponsor_address                 = post('sponsor_address');
                $skck->sponsor_phone                   = post('sponsor_phone');
                $skck->sponsor_business                = post('sponsor_business');
            }

            $skck->purpose                         = post('purpose');
            $skck->job_description                 = post('job_description');
            $skck->hobby                           = post('hobby');
            $skck->address                         = post('phone_address');

            if(post('is_marriage')) {
                $skck->relation                        = post('is_marriage');
                $skck->relation_name                   = post('relation_name');
                $skck->relation_age                    = post('relation_age');
                $skck->relation_religion               = post('relation_religion');
                $skck->relation_nationality            = post('relation_nationality');
                $skck->relation_job                    = post('relation_job');
                $skck->relation_address                = post('relation_address');
            }

            $skck->father_name                     = post('father_name');
            $skck->father_age                      = post('father_age');
            $skck->father_religion                 = post('father_religion');
            $skck->father_nationality              = post('father_nationality');
            $skck->father_job                      = post('father_job');
            $skck->father_address                  = post('father_address');

            $skck->mother_name                     = post('mother_name');
            $skck->mother_age                      = post('mother_age');
            $skck->mother_religion                 = post('mother_religion');
            $skck->mother_nationality              = post('mother_nationality');
            $skck->mother_job                      = post('mother_job');
            $skck->mother_address                  = post('mother_address');

            $skck->siblings0name                   = post('siblings0name');
            $skck->siblings0age                    = post('siblings0age');
            $skck->siblings0job                    = post('siblings0job');
            $skck->siblings0address                = post('siblings0address');
            $skck->siblings1name                   = post('siblings1name');
            $skck->siblings1age                    = post('siblings1age');
            $skck->siblings1job                    = post('siblings1job');
            $skck->siblings1address                = post('siblings1address');
            $skck->siblings2name                   = post('siblings2name');
            $skck->siblings2age                    = post('siblings2age');
            $skck->siblings2job                    = post('siblings2job');
            $skck->siblings2address                = post('siblings2address');
            $skck->siblings3name                   = post('siblings3name');
            $skck->siblings3age                    = post('siblings3age');
            $skck->siblings3job                    = post('siblings3job');
            $skck->siblings3address                = post('siblings3address');

            $skck->schools0name                    = post('schools0name');
            $skck->schools0year                    = post('schools0year');
            $skck->schools1name                    = post('schools1name');
            $skck->schools1year                    = post('schools1year');
            $skck->schools2name                    = post('schools2name');
            $skck->schools2year                    = post('schools2year');
            $skck->schools3name                    = post('schools3name');
            $skck->schools3year                    = post('schools3year');

            $skck->save();

            $requestItem->requestable_id = $skck->id;
            $requestItem->save();
        }


        /**
         * Tnkb Table
        */
        if($service->code == 'permit-tnkb') {
            if(post('type') == 'tnkb-yearly') {
                $tnkb          = new RequestTnkbYearlyModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if(post('type') == 'tnkb-renewal') {
                $tnkb          = new RequestTnkbRenewalModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if(post('type') == 'tnkb-lost') {
                $tnkb          = new RequestTnkbLostModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if(post('type') == 'tnkb-reunited') {
                $tnkb          = new RequestTnkbReunitedModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if(post('type') == 'tnkb-out') {
                $tnkb          = new RequestTnkbOutModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if(post('type') == 'tnkb-in') {
                $tnkb          = new RequestTnkbInModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }
        }

        /**
         * Crowd Table
        */
        if($service->code == 'permit-crowd') {
            if(post('type') == 'crowd') {
                $crowd              = new RequestCrowdModels;
                $crowd->item_id     = $requestItem->id;
                $crowd->kind        = post('kind');
                $crowd->place       = post('place');
                $crowd->participant = post('participant');
                $crowd->save();

                $requestItem->requestable_id = $crowd->id;
                $requestItem->save();
            }

            if(post('type') == 'crowd-firework') {
                $crowd          = new RequestCrowdFireworkModels;
                $crowd->item_id = $requestItem->id;
                $crowd->purpose = post('purpose');
                $crowd->qty     = post('qty');
                $crowd->source  = post('source');
                $crowd->kind    = post('kind');
                $crowd->date    = post('date');
                $crowd->time    = post('time');
                $crowd->pic     = post('pic');
                $crowd->save();

                $requestItem->requestable_id = $crowd->id;
                $requestItem->save();
            }

            if(post('type') == 'crowd-public') {
                $crowd              = new RequestCrowdPublicModels;
                $crowd->item_id     = $requestItem->id;
                $crowd->kind        = post('kind');
                $crowd->date        = post('date');
                $crowd->time        = post('time');
                $crowd->place       = post('place');
                $crowd->save();

                $requestItem->requestable_id = $crowd->id;
                $requestItem->save();
            }

            if(post('type') == 'crowd-campaign') {
                $crowd                   = new RequestCrowdCampaignModels;
                $crowd->item_id          = $requestItem->id;
                $crowd->campaign_group   = post('campaign_group');
                $crowd->campaign_address = post('campaign_address');
                $crowd->kind             = post('kind');
                $crowd->date             = post('date');
                $crowd->time             = post('time');
                $crowd->place            = post('place');
                $crowd->participant      = post('participant');
                $crowd->lead             = post('lead');
                $crowd->vehicle          = post('vehicle');
                $crowd->apk              = post('apk');
                $crowd->save();

                $requestItem->requestable_id = $crowd->id;
                $requestItem->save();
            }
        }

        /**
         * Hearse Table
        */
        if($service->code == 'permit-hearse') {
            if(post('type') == 'hearse') {
                $hearse                          = new RequestHearseModels;
                $hearse->item_id                 = $requestItem->id;
                $hearse->scheduled_at            = Carbon::parse(post('appointment'))->format('Y-m-d H:i:s');
                $hearse->origin_regency_id       = post('origin_regency_id');
                $hearse->origin_district_id      = post('origin_district_id');
                $hearse->origin_village_id       = post('origin_village_id');
                $hearse->origin_rt               = post('origin_rt');
                $hearse->origin_rw               = post('origin_rw');
                $hearse->origin_address          = post('origin_address');
                $hearse->destination_regency_id  = post('destination_regency_id');
                $hearse->destination_district_id = post('destination_district_id');
                $hearse->destination_village_id  = post('destination_village_id');
                $hearse->destination_rt          = post('destination_rt');
                $hearse->destination_rw          = post('destination_rw');
                $hearse->destination_address     = post('destination_address');
                $hearse->save();

                $requestItem->requestable_id = $hearse->id;
                $requestItem->save();
            }

            if(post('type') == 'hearse-other') {
                $hearse                      = new RequestHearseOtherModels;
                $hearse->item_id             = $requestItem->id;
                $hearse->scheduled_at        = Carbon::parse(post('appointment'))->format('Y-m-d H:i:s');

                $hearse->type                = post('hearse_type');
                $hearse->origin_address      = post('origin_address');
                $hearse->origin_rt           = post('origin_rt');
                $hearse->origin_rw           = post('origin_rw');
                $hearse->destination_address = post('destination_address');
                $hearse->destination_rt      = post('destination_rt');
                $hearse->destination_rw      = post('destination_rw');
                $hearse->save();

                $requestItem->requestable_id = $hearse->id;
                $requestItem->save();
            }
        }

        /**
         * Drug Table
        */
        if($service->code == 'permit-drug') {
            if(post('type') == 'drug-new') {
                $drug               = new RequestDrugModels;
                $drug->item_id      = $requestItem->id;
                $drug->header_doctor= post('header_doctor');
                $drug->purpose      = post('purpose');
                $drug->save();

                $requestItem->requestable_id = $drug->id;
                $requestItem->save();
            }

            if(post('type') == 'drug-extend') {
                $drug               = new RequestDrugModels;
                $drug->item_id      = $requestItem->id;
                $drug->header_doctor= post('header_doctor');
                $drug->purpose      = post('purpose');
                $drug->save();

                $requestItem->requestable_id = $drug->id;
                $requestItem->save();
            }
        }

        /**
         * Finger Table
        */
        if($service->code == 'permit-finger') {
            if(post('type') == 'finger-new') {
                $finger          = new RequestFingerModels;
                $finger->item_id = $requestItem->id;
                $finger->save();

                $requestItem->requestable_id = $finger->id;
                $requestItem->save();
            }
        }

        /**
         * Lost Table
        */
        if($service->code == 'permit-lost') {
            if(post('type') == 'lost-new') {
                $lost              = new RequestLostModels;
                $lost->item_id     = $requestItem->id;
                $lost->kind        = post('kind');
                $lost->content     = post('content');
                $lost->date        = post('date');
                $lost->time        = post('time');
                $lost->place       = post('place');
                $lost->description = post('description');
                $lost->save();

                $requestItem->requestable_id = $lost->id;
                $requestItem->save();
            }
        }

        if($service->code == 'permit-pick') {
            if(post('type') == 'pick-sick') {
                $pick                          = new RequestPickSickModels;
                $pick->item_id                 = $requestItem->id;
                $pick->scheduled_at            = Carbon::parse(post('appointment'))->format('Y-m-d H:i:s');
                $pick->origin_regency_id       = post('origin_regency_id');
                $pick->origin_district_id      = post('origin_district_id');
                $pick->origin_village_id       = post('origin_village_id');
                $pick->origin_rt               = post('origin_rt');
                $pick->origin_rw               = post('origin_rw');
                $pick->origin_address          = post('origin_address');
                $pick->destination_regency_id  = post('destination_regency_id');
                $pick->destination_district_id = post('destination_district_id');
                $pick->destination_village_id  = post('destination_village_id');
                $pick->destination_rt          = post('destination_rt');
                $pick->destination_rw          = post('destination_rw');
                $pick->destination_address     = post('destination_address');
                $pick->save();

                $requestItem->requestable_id   = $pick->id;
                $requestItem->save();
            }
        }

        return response()->json([
            'result'  => true,
            'request' => [
                'data' => $request
            ]
        ]);
    }

    public function storeAppointment()
    {
        $kk    = Carbon::parse(post('date'))->format('Y-m-d');
        $rules = [
            'date'        => 'required|date|after:'.date('Y-m-d')
        ];
        $messages       = [];
        $attributeNames = [
            'date'        => 'tanggal kehadiran',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $date         = post('date');
        $chose        = Carbon::parse($date)->format('l');

        if(strtolower($chose) == 'saturday') {
            $chose = 'sabtu';
        }
        if(strtolower($chose) == 'sunday') {
            $chose = 'minggu';
        }

        $disableDates = ['saturday', 'sunday', 'sabtu', 'minggu'];
        if(in_array($chose, $disableDates)) {
            return response()->json([
                'message' => 'Kehadiran tidak tersedia pada hari '.$chose
            ]);
        }

        return response()->json([
            'result'        => true,
            'appointment'   => $date
        ]);
    }

    public function storeAppointmentHearse()
    {
        $kk    = Carbon::parse(post('date'))->format('Y-m-d');
        $rules = [
            'date'        => 'required|date|after:'.date('Y-m-d')
        ];
        $messages       = [];
        $attributeNames = [
            'date'        => 'tanggal pengawalan',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $date         = post('date');
        return response()->json([
            'result'        => true,
            'appointment'   => $date
        ]);
    }

    public function storeCustomer()
    {
        $rules = [
            'name'        => 'required',
            'phone'       => 'required|numeric',
            'email'       => 'required|email',
            'gender'      => 'required',
            'job'         => 'required',
            'nationality' => 'required',
            'address'     => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'phone'       => 'telepon',
            'email'       => 'email',
            'gender'      => 'jenis kelamin',
            'job'         => 'pekerjaan',
            'nationality' => 'kewarganegaraan',
            'address'     => 'alamat',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        return response()->json([
            'result'    => true,
            'customer'  => [
                'data' => post()
            ]
        ]);
    }

    public function storeCustomerSkck()
    {
        $rules = [
            'name'        => 'required',
            'place'       => 'required',
            'dob'         => 'required',
            'phone'       => 'required|numeric',
            'email'       => 'required|email',
            'gender'      => 'required',
            'religion'    => 'required',
            'is_marriage' => 'required',
            'job'         => 'required',
            'nationality' => 'required',
            'address'     => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'place'       => 'tempat lahir',
            'dob'         => 'tanggal lahir',
            'phone'       => 'telepon',
            'email'       => 'email',
            'gender'      => 'jenis kelamin',
            'is_marriage' => 'status perkawinan',
            'job'         => 'pekerjaan',
            'nationality' => 'kewarganegaraan',
            'address'     => 'alamat',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        return response()->json([
            'result'    => true,
            'customer'  => [
                'data' => post()
            ]
        ]);
    }

    /**
     * [storeSimBase description]
     * @return [type] [description]
     */
    public function storeSimBase()
    {
        $rules = [
            'personalHeight'         => 'required',
            'personalEducation'      => 'required',
            'parentFather'           => 'required',
            'parentMother'           => 'required',
            'personalAddressCity'    => 'required',
            'personalAddressVillage' => 'required',
            'personalAddressRt'      => 'required',
            'personalAddressRw'      => 'required',
            'personalAddressPos'     => 'required',
            'hasGlasses'             => 'required',
            'hasPsy'                 => 'required',
            'hasCertified'           => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'personalHeight'         => 'tinggi badan',
            'personalEducation'      => 'pendidikan terakhir ',
            'parentFather'           => 'nama ayah',
            'parentMother'           => 'nama ibu',
            'personalAddressCity'    => 'kota/kabupaten',
            'personalAddressVillage' => 'kelurahan',
            'personalAddressRt'      => 'rt',
            'personalAddressRw'      => 'rw',
            'personalAddressPos'     => 'kode pos',
            'hasGlasses'             => 'kacamata',
            'hasPsy'                 => 'cacat fisik',
            'hasCertified'           => 'sertifikat',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    /**
     * Handle Sim Purpose
     * @return [type] [description]
    */
    public function storeSimStudent()
    {
        $rules = [
            'hasStudent'        => 'required|in:true',
            'hasStudentCopy'    => 'required|in:true',
            'hasHealthy'        => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimC()
    {
        $rules = [
            'hasKtp'     => 'required|in:true',
            'hasKtpCopy' => 'required|in:true',
            'hasHealthy' => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimCExtend()
    {
        $rules = [
            'hasSim'     => 'required|in:true',
            'hasKtp'     => 'required|in:true',
            'hasKtpCopy' => 'required|in:true',
            'hasHealthy' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimA()
    {
        $rules = [
            'hasKtp'     => 'required|in:true',
            'hasKtpCopy' => 'required|in:true',
            'hasHealthy' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimAExtend()
    {
        $rules = [
            'hasSim'     => 'required|in:true',
            'hasKtp'     => 'required|in:true',
            'hasKtpCopy' => 'required|in:true',
            'hasHealthy' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimAUmum()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasA'          => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimAUmumExtend()
    {
        $rules = [
            'hasSim'        => 'required|in:true',
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimB1()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasA'          => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimB1Extend()
    {
        $rules = [
            'hasSim'        => 'required|in:true',
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimB1Umum()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasA'          => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimB1UmumExtend()
    {
        $rules = [
            'hasSim'        => 'required|in:true',
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimB2()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasB'          => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimB2Extend()
    {
        $rules = [
            'hasSim'        => 'required|in:true',
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimB2Umum()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasB'          => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSimB2UmumExtend()
    {
        $rules = [
            'hasSim'        => 'required|in:true',
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasHealthy'    => 'required|in:true',
            'hasPsychology' => 'required|in:true'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'sim'    => [
                'data' => post()
            ]
        ]);
    }


    /**
     * Handle Skck Purpose
     * @return [type] [description]
     */
    public function storeSkckNew()
    {
        $rules = [
            'hasKtp'    => 'required|in:true',
            'hasKk'     => 'required|in:true',
            'hasAkta'   => 'required|in:true',
            'hasPaspor' => 'required',
            'hasPhoto'  => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                // 'message' => $validator->messages()->first(),
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'skck'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSkckFamily()
    {
        $rules          = [];
        $messages       = [];
        $attributeNames = [];

        $rules['purpose']                  = 'required';
        $attributeNames['purpose']         = 'tujuan pembuatan';
        $rules['job_description']          = 'required';
        $attributeNames['job_description'] = 'pengalaman kerja';
        $rules['hobby']                    = 'required';
        $attributeNames['hobby']           = 'kegemaran atau hobi';
        $rules['phone_address']            = 'required';
        $attributeNames['phone_address']   = 'nomor yang mudah dihubungi';

        if(post('is_marriage')) {
            $rules['relation_name']                 = 'required';
            $attributeNames['relation_name']        = 'nama suami/istri';
            $rules['relation_age']                  = 'required';
            $attributeNames['relation_age']         = 'umur suami/istri';
            $rules['relation_religion']             = 'required';
            $attributeNames['relation_religion']    = 'agama suami/istri';
            $rules['relation_nationality']          = 'required';
            $attributeNames['relation_nationality'] = 'kebangsaan suami/istri';
            $rules['relation_job']                  = 'required';
            $attributeNames['relation_job']         = 'pekerjaan suami/istri';
            $rules['relation_address']              = 'required';
            $attributeNames['relation_address']     = 'alamat suami/istri';
        }

        $rules['father_name']                 = 'required';
        $attributeNames['father_name']        = 'nama bapak';
        $rules['father_age']                  = 'required';
        $attributeNames['father_age']         = 'umur bapak';
        $rules['father_religion']             = 'required';
        $attributeNames['father_religion']    = 'agama bapak';
        $rules['father_nationality']          = 'required';
        $attributeNames['father_nationality'] = 'kebangsaan bapak';
        $rules['father_job']                  = 'required';
        $attributeNames['father_job']         = 'pekerjaan bapak';
        $rules['father_address']              = 'required';
        $attributeNames['father_address']     = 'alamat bapak';

        $rules['mother_name']                 = 'required';
        $attributeNames['mother_name']        = 'nama ibu';
        $rules['mother_age']                  = 'required';
        $attributeNames['mother_age']         = 'umur ibu';
        $rules['mother_religion']             = 'required';
        $attributeNames['mother_religion']    = 'agama ibu';
        $rules['mother_nationality']          = 'required';
        $attributeNames['mother_nationality'] = 'kebangsaan ibu';
        $rules['mother_job']                  = 'required';
        $attributeNames['mother_job']         = 'pekerjaan ibu';
        $rules['mother_address']              = 'required';
        $attributeNames['mother_address']     = 'alamat ibu';

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first(),
                // 'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'skck'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSkckRegistration()
    {
        $rules          = [];
        $messages       = [];
        $attributeNames = [];

        if(post('is_criminal') == 'true') {
            $rules['criminal_case']                   = 'required';
            $rules['criminal_punishment_description'] = 'required';
            $rules['criminal_punishment']             = 'required';
            $rules['criminal_punishment_process']     = 'required';
        }

        if(post('is_violation') == 'true') {
            $rules['violation_case']                   = 'required';
            $rules['vionlation_punishment']            = 'required';
        }

        if(post('is_sponsor') == 'true') {
            $rules['sponsor']                          = 'required';
            $rules['sponsor_address']                  = 'required';
            $rules['sponsor_phone']                    = 'required';
            $rules['sponsor_business']                 = 'required';
        }

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                // 'message' => $validator->messages()->first(),
                'message' => 'Anda wajib mengisi kolom input yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result'       => true,
            'skckCrime'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeSkckExtend()
    {
        $rules = [
            'hasSkck'   => 'required|in:true',
            'hasFinger' => 'required|in:true',
            'hasKtp'    => 'required|in:true',
            'hasKk'     => 'required|in:true',
            'hasAkta'   => 'required|in:true',
            'hasPaspor' => 'required',
            'hasPhoto'  => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'skck'    => [
                'data' => post()
            ]
        ]);
    }

    /**
     * Handle Tnkb Purpose
     * @return [type] [description]
     */
    public function storeTnkbYearly()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasStnk'       => 'required|in:true',
            'hasStnkCopy'   => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'tnkb'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeTnkbRenewal()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasStnk'       => 'required|in:true',
            'hasStnkCopy'   => 'required|in:true',
            'hasBpkb'       => 'required|in:true',
            'hasBpkbCopy'   => 'required|in:true',
            'hasPhysical'   => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'tnkb'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeTnkbLost()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasStnk'       => 'required|in:true',
            'hasBpkb'       => 'required|in:true',
            'hasBpkbCopy'   => 'required|in:true',
            'hasLost'       => 'required|in:true',
            'hasPhysical'   => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'tnkb'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeTnkbReunited()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasStnk'       => 'required|in:true',
            'hasBpkb'       => 'required|in:true',
            'hasBpkbCopy'   => 'required|in:true',
            'hasKtpNew'     => 'required|in:true',
            'hasPurchase'   => 'required|in:true',
            'hasPhysical'   => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'tnkb'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeTnkbOut()
    {
        $rules = [
            'hasKtp'        => 'required|in:true',
            'hasPurchase'   => 'required|in:true',
            'hasStnk'       => 'required|in:true',
            'hasStnkCopy'   => 'required|in:true',
            'hasBpkb'       => 'required|in:true',
            'hasBpkbCopy'   => 'required|in:true',
            'hasPhysical'   => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'tnkb'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeTnkbIn()
    {
        $rules = [
            'hasPurchase'   => 'required|in:true',
            'hasKtp'        => 'required|in:true',
            'hasKtpCopy'    => 'required|in:true',
            'hasStnk'       => 'required|in:true',
            'hasStnkCopy'   => 'required|in:true',
            'hasBpkb'       => 'required|in:true',
            'hasBpkbCopy'   => 'required|in:true',
            'hasPhysical'   => 'required|in:true',
            'hasMoveLetter' => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'tnkb'    => [
                'data' => post()
            ]
        ]);
    }

    /**
     * Handle Drug Purpose
     * @return [type] [description]
     */
    public function storeDrugNew()
    {
        $rules = [
            'headerDoctor' => 'required',
            'hasKtp'       => 'required|in:true',
            'hasKk'        => 'required|in:true',
            'hasUrine'     => 'required|in:true',
            'hasPhoto'     => 'required|in:true',
            'purpose'      => 'required'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'drug'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeDrugExtend()
    {
        $rules = [
            'hasDrug'      => 'required|in:true',
            'headerDoctor' => 'required',
            'hasKtp'       => 'required|in:true',
            'hasKk'        => 'required|in:true',
            'hasUrine'     => 'required|in:true',
            'hasPhoto'     => 'required|in:true',
            'purpose'      => 'required'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'drug'    => [
                'data' => post()
            ]
        ]);
    }

    /**
     * Crowd Purpose
     * @return [type] [description]
     */
    public function storeCrowd()
    {
        $rules = [
            'hasKtp'      => 'required|in:true',
            'hasProposal' => 'required|in:true',
            'kind'        => 'required',
            'place'       => 'required',
            'participant' => 'required'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'crowd'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeCrowdFirework()
    {
        $rules = [
            'hasPermitPlace'  => 'required|in:true',
            'hasPermitPolice' => 'required|in:true',

            'purpose'         => 'required',
            'qty'             => 'required',
            'source'          => 'required',
            'kind'            => 'required',
            'date'            => 'required',
            'time'            => 'required',
            'pic'             => 'required'
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'crowd'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeCrowdPublic()
    {
        $rules = [
            'hasProposal'      => 'required|in:true',
            'hasPermitRequest' => 'required|in:true',
            'hasPermitPlace'   => 'required|in:true',

            'kind'             => 'required',
            'date'             => 'required',
            'time'             => 'required',
            'place'            => 'required',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'crowd'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeCrowdCampaign()
    {
        $rules = [
            'hasProposal'      => 'required|in:true',
            'hasPermitRequest' => 'required|in:true',
            'hasPermitPlace'   => 'required|in:true',

            'campaign_group'   => 'required',
            'campaign_address' => 'required',
            'kind'             => 'required',
            'date'             => 'required',
            'time'             => 'required',
            'place'            => 'required',
            'participant'      => 'required',
            'lead'             => 'required',
            'vehicle'          => 'required',
            'apk'              => 'required',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'crowd'    => [
                'data' => post()
            ]
        ]);
    }

    /**
     * Hearse purpose
     * @return [type] [description]
     */
    public function storeHearse()
    {
        $rules = [
            'origin_regency_id'       => 'required',
            'origin_district_id'      => 'required',
            'origin_village_id'       => 'required',
            'origin_rt'               => 'required',
            'origin_rw'               => 'required',
            'origin_address'          => 'required',
            'destination_regency_id'  => 'required',
            'destination_district_id' => 'required',
            'destination_village_id'  => 'required',
            'destination_rt'          => 'required',
            'destination_rw'          => 'required',
            'destination_address'     => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'origin_regency_id'       => 'asal kota/kabupaten',
            'origin_district_id'      => 'asal kecamatan',
            'origin_village_id'       => 'asal kelurahan',
            'origin_rt'               => 'asal rt',
            'origin_rw'               => 'asal rt',
            'origin_address'          => 'asal alamat',
            'destination_regency_id'  => 'tujuan kota/kabupaten',
            'destination_district_id' => 'tujuan kecamatan',
            'destination_village_id'  => 'tujuan kelurahan',
            'destination_rt'          => 'tujuan rt',
            'destination_rw'          => 'tujuan rw',
            'destination_address'     => 'tujuan alamat',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'hearse'    => [
                'data' => post()
            ]
        ]);
    }

    public function storeHearseOther()
    {
        $rules = [
            'type'                => 'required',
            'origin_address'      => 'required',
            'origin_rt'           => 'required',
            'origin_rw'           => 'required',
            'destination_address' => 'required',
            'destination_rt'      => 'required',
            'destination_rw'      => 'required',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'hearse'    => [
                'data' => post()
            ]
        ]);
    }


    /**
     * [storeFingerNew description]
     * @return [type] [description]
     */
    public function storeFingerNew()
    {
        $rules = [
            'hasKtp'   => 'required|in:true',
            'hasPhoto' => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'finger' => [
                'data' => post()
            ]
        ]);
    }

    /**
     * [storeFingerNew description]
     * @return [type] [description]
     */
    public function storeLostNew()
    {
        $rules = [
            'kind'        => 'required',
            'content'     => 'required',
            'date'        => 'required',
            'time'        => 'required',
            'place'       => 'required',
            'description' => 'required',
            'hasKtp'      => 'required|in:true',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Anda wajib memiliki seluruh persyaratan yang dibutuhkan'
            ]);
        }

        return response()->json([
            'result' => true,
            'lost'   => [
                'data' => post()
            ]
        ]);
    }
}
