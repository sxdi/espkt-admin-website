<?php namespace Pulangpisau\Api\Controllers;

use Hash;
use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Pulangpisau\Api\Transformer\RequestItemTransformer;

use Pulangpisau\Request\Models\RequestItem as RequestItemModels;
use Pulangpisau\Request\Models\RequestFeedback as RequestFeedbackModels;

class Feedback extends Controller
{
    public function get()
    {
    	$fractal    = new Fractal\Manager();
    	if (isset($_GET['include'])) {
            $fractal->parseIncludes($_GET['include']);
        }

        $code    = input('code');
        $request = RequestItemModels::whereHas('request', function($q) use($code) {
            $q->whereCode($code);
        })->doesnthave('feedback')->first();

        if(!count($request)) {
            return response()->json([
                'message' => 'Permohonan tidak ditemukan'
            ]);
        }

        return response()->json([
            'result'   => true,
            'requests' => $fractal->createData(new Fractal\Resource\Item($request, new RequestItemTransformer))->toArray()
        ]);
    }

    public function store()
    {
        $rules = [
            'rate' => 'required'
        ];
        $messages       = [];
        $attributeNames = [
            'rate' => 'nilai kepuasan'
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $requestFeedback = RequestFeedbackModels::firstOrCreate([
            'id'      => post('id'),
            'item_id' => post('item_id')
        ]);
        $requestFeedback->rate    = post('rate');
        $requestFeedback->comment = post('comment');
        $requestFeedback->save();

        return response()->json([
            'result' => true
        ]);
    }
}
