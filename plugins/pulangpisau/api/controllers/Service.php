<?php namespace Pulangpisau\Api\Controllers;

use Hash;
use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Pulangpisau\Api\Transformer\ServiceTransformer;

use Pulangpisau\Service\Models\Service as ServiceModels;
use Pulangpisau\Service\Models\ServiceItem as ServiceItemModels;

class Service extends Controller
{

    public function get()
    {
        $fractal   = new Fractal\Manager();

        if(!input('code')) {
	        $service   = ServiceModels::whereNull('parent_id')->get();

	        return response()->json([
	            'result'  => true,
	            'services' => $fractal->createData(new Fractal\Resource\Collection($service, new ServiceTransformer))->toArray()
	        ]);
	    }

	    $service = ServiceModels::whereCode(input('code'))->first();
	    return response()->json([
            'result'  => true,
            'service' => $fractal->createData(new Fractal\Resource\Item($service, new ServiceTransformer))->toArray()
        ]);
    }

    public function getRequest()
    {
        $service = ServiceModels::whereCode(input('service'))->first();
        return response()->json([
            'result'  => true,
            'service' => [
                'data' => $service->childs
            ]
        ]);
    }

    public function getRequestQueue()
    {
        $service = ServiceModels::whereId(input('service'))->first();
        return response()->json([
            'result'  => true,
            'service' => [
                'data' => $service->childs
            ]
        ]);
    }

    public function item()
    {
        if(input('code')) {
            $item = ServiceItemModels::whereCode(input('code'))->first();
            return response()->json([
                'result' => true,
                'item'   => [
                    'video' => $item->video->getPath(),
                    'pdf'   => $item->pdf->getPath()
                ]
            ]);
        }

        if(input('student')) {
            $item = ServiceItemModels::whereIn('id', [2,3])->get();
            return response()->json([
                'result' => true,
                'items'  => [
                    'data' => $item
                ]
            ]);
        }

        $item = ServiceItemModels::get();
        return response()->json([
            'result' => true,
            'items'  => [
                'data' => $item
            ]
        ]);
    }
}
