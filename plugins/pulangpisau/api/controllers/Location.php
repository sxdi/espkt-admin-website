<?php namespace Pulangpisau\Api\Controllers;

use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Pulangpisau\Location\Models\Regency as RegencyModels;
use Pulangpisau\Location\Models\District as DistrictModels;
use Pulangpisau\Location\Models\Village as VillageModels;

class Location extends Controller
{
    /**
     * Origin Purpose
     * @return [type] [description]
    */
    public function getOriginRegency()
    {
        return response()->json([
            'result'        => true,
            'OriginRegency' => [
                'data' => RegencyModels::whereProvinceId(62)->orderBy('name', 'asc')->get()
            ]
        ]);
    }

    public function getOriginDistrict()
    {
        return response()->json([
            'result'         => true,
            'OriginDistrict' => [
                'data' => DistrictModels::whereRegencyId(input('regency'))->orderBy('name', 'asc')->get()
            ]
        ]);
    }

    public function getOriginVillage()
    {
        return response()->json([
            'result'         => true,
            'OriginVillage'  => [
                'data' => VillageModels::whereDistrictId(input('district'))->orderBy('name', 'asc')->get()
            ]
        ]);
    }


    /**
     * Destinatin Purpose
     * @return [type] [description]
    */
    public function getDestinationRegency()
    {
        return response()->json([
            'result'             => true,
            'DestinationRegency' => [
                'data' => RegencyModels::whereProvinceId(62)->orderBy('name', 'asc')->get()
            ]
        ]);
    }

    public function getDestinationDistrict()
    {
        return response()->json([
            'result'              => true,
            'DestinationDistrict' => [
                'data' => DistrictModels::whereRegencyId(input('regency'))->orderBy('name', 'asc')->get()
            ]
        ]);
    }

    public function getDestinationVillage()
    {
        return response()->json([
            'result'              => true,
            'DestinationVillage'  => [
                'data' => VillageModels::whereDistrictId(input('district'))->orderBy('name', 'asc')->get()
            ]
        ]);
    }
}
