<?php namespace Pulangpisau\Api\Controllers;

use Hash;
use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Pulangpisau\Section\Models\Section as SectionModels;

class Section extends Controller
{

    public function get()
    {
        $fractal   = new Fractal\Manager();

        if(!input('code')) {
            $section   = SectionModels::get();

            return response()->json([
                'result'   => true,
                'sections' => [
                    'data' => $section
                ]
            ]);
        }

        $section = SectionModels::whereCode(input('code'))->first();
        return response()->json([
            'result'  => true,
            'section' => [
                'data' => $section
            ]
        ]);
    }
}
