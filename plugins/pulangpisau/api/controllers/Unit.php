<?php namespace Pulangpisau\Api\Controllers;

use Hash;
use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Pulangpisau\Unit\Models\Unit as UnitModels;

class Unit extends Controller
{

    public function get()
    {
        $fractal   = new Fractal\Manager();

        if(!input('code')) {
            $unit   = UnitModels::get();

            return response()->json([
                'result'   => true,
                'units' => [
                    'data' => $unit
                ]
            ]);
        }

        $unit = UnitModels::whereCode(input('code'))->first();
        return response()->json([
            'result'  => true,
            'unit' => [
                'data' => $unit
            ]
        ]);
    }
}
