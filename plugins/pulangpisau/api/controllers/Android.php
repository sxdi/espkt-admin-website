<?php namespace Pulangpisau\Api\Controllers;

use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Pulangpisau\Setting\Models\Setting as AdroidSettings;

class Android extends Controller
{
    public function get()
    {
        $k = \Pulangpisau\Setting\Models\Setting::instance();
        return response()->json([
            'result'  => true,
            'banner'  => $k->banner_primary ? $k->banner_primary->path : '',
            'map'     => $k->map_primary ? $k->map_primary->path : '',
            'version' => (int) 102
        ]);
    }
}


