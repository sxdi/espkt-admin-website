<?php namespace Pulangpisau\Api\Controllers;

use Hash;
use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Pulangpisau\Counter\Models\Counter as CounterModels;

class Counter extends Controller
{

    public function get()
    {
        $fractal   = new Fractal\Manager();

        if(!input('code')) {
            $counter   = CounterModels::orderBy('code')->get();

            return response()->json([
                'result'   => true,
                'counters' => [
                    'data' => $counter
                ]
            ]);
        }

        $counter   = CounterModels::whereCode(input('code'))->get();
        return response()->json([
            'result'   => true,
            'counter' => [
                'data' => $counter
            ]
        ]);
    }
}
