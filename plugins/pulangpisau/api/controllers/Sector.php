<?php namespace Pulangpisau\Api\Controllers;

use Hash;
use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Pulangpisau\Sector\Models\Sector as SectorModels;

class Sector extends Controller
{

    public function get()
    {
        $fractal   = new Fractal\Manager();

        if(!input('code')) {
            $sector   = SectorModels::get();

            return response()->json([
                'result'   => true,
                'sectors' => [
                    'data' => $sector
                ]
            ]);
        }

        $sector = SectorModels::whereSlug(input('code'))->first();
        return response()->json([
            'result'  => true,
            'sector' => [
                'data' => $sector
            ]
        ]);
    }
}
