<?php namespace Pulangpisau\Api\Controllers;

use Pusher;
use Hash;
use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Pulangpisau\Api\Transformer\QueueTransformer;

use Pulangpisau\Service\Models\Service as ServiceModels;

use Pulangpisau\Queue\Models\Queue as QueueModels;

use Pulangpisau\Request\Models\Request as RequestModels;

class Queue extends Controller
{
    public function getService($code)
    {
        return ServiceModels::whereId($code)->first();
    }

    public function store()
    {
        $fractal   = new Fractal\Manager();
        $service   = $this->getService(post('service'));
        $queueLast = QueueModels::whereDate('created_at', '=', date('Y-m-d'))->whereCounterId($service->counter->counter_id)->orderBy('id', 'desc')->first();

        $queue             = new QueueModels;
        $queue->counter_id = $service->counter->counter_id;
        $queue->number     = $queueLast ? $queueLast->number + 1 : 1;
        $queue->status     = 'waiting';
        $queue->is_online  = 0;
        $queue->is_coming  = 1;
        $queue->save();

        return response()->json([
            'result'  => true,
            'service' => $service->name,
            'queue'   => $fractal->createData(new Fractal\Resource\Item($queue, new QueueTransformer))->toArray()
        ]);
    }

    public function call()
    {
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher\Pusher(
            '1e534418562e080bed18',
            '86fcd17702614837b361',
            '840572',
        $options
        );

        $data['channel'] = post('channel');
        $data['number']  = post('number');
        $pusher->trigger('queue-channel', 'queue-call', $data);
        return response()->json([
            'result'  => true,
            'message' => 'Memanggil nomor '.post('number').' ke loket '.strtoupper(post('channel'))
        ]);
    }

    public function attend()
    {
        $request = RequestModels::whereCode(input('code'))->first();
        if($request) {
            QueueModels::whereId($request->items[0]->queue->queueRequest->id)->update([
                'is_coming' => 1
            ]);
            return response()->json([
                'result' => true
            ]);
        }

        return response()->json([
            'result' => false
        ]);
    }
}
