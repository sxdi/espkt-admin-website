<?php

Route::group([
    'prefix'    => 'v1',
    'namespace' => 'Pulangpisau\Api\Controllers',

    ], function() {
        /**
         * Android
        */
        Route::group(['prefix' => 'android'], function() {
            Route::get('/', 'Android@get');
        });

        /**
         * Android
        */
        Route::group(['prefix' => 'counter'], function() {
            Route::get('/', 'Counter@get');
        });

        /**
         * Service
        */
        Route::group(['prefix' => 'service'], function() {
            Route::get('/', 'Service@get');
            Route::get('/kind', 'Service@item');
            Route::get('/request', 'Service@getRequest');
            Route::get('/request/queue', 'Service@getRequestQueue');
        });

        /**
         * Request
        */
        Route::group(['prefix' => 'request'], function() {
            Route::post('', 'Request@store');
            Route::post('customer', 'Request@storeCustomer');
            Route::post('customer/skck', 'Request@storeCustomerSkck');
            Route::post('appointment', 'Request@storeAppointment');
            Route::post('appointment/hearse', 'Request@storeAppointmentHearse');

            Route::group(['prefix' => 'sim'], function() {
                Route::post('student',        'Request@storeSimStudent');
                Route::post('base',           'Request@storeSimBase');

                Route::group(['prefix' => 'new'], function() {
                    Route::post('c',        'Request@storeSimC');
                    Route::post('a',        'Request@storeSimA');
                    Route::post('a-umum',   'Request@storeSimAUmum');
                    Route::post('b1',       'Request@storeSimB1');
                    Route::post('b1-umum',  'Request@storeSimB1Umum');
                    Route::post('b2',       'Request@storeSimB2');
                    Route::post('b2-umum',  'Request@storeSimB2Umum');
                });

                Route::group(['prefix' => 'extend'], function() {
                    Route::post('c',        'Request@storeSimCExtend');
                    Route::post('a',        'Request@storeSimAExtend');
                    Route::post('a-umum',   'Request@storeSimAUmumExtend');
                    Route::post('b1',       'Request@storeSimB1Extend');
                    Route::post('b1-umum',  'Request@storeSimB1UmumExtend');
                    Route::post('b2',       'Request@storeSimB2Extend');
                    Route::post('b2-umum',  'Request@storeSimB2UmumExtend');
                });
            });

            Route::group(['prefix' => 'skck'], function() {
                Route::post('new',              'Request@storeSkckNew');
                Route::post('new-family',       'Request@storeSkckFamily');
                Route::post('new-registration', 'Request@storeSkckRegistration');
                Route::post('extend',           'Request@storeSkckExtend');
            });

            Route::group(['prefix' => 'tnkb'], function() {
                Route::post('yearly', 'Request@storeTnkbYearly');
                Route::post('renewal', 'Request@storeTnkbRenewal');
                Route::post('lost', 'Request@storeTnkbLost');
                Route::post('reunited', 'Request@storeTnkbReunited');
                Route::post('out', 'Request@storeTnkbOut');
                Route::post('in', 'Request@storeTnkbIn');
            });


            Route::group(['prefix' => 'drug'], function() {
                Route::post('new', 'Request@storeDrugNew');
                Route::post('extend', 'Request@storeDrugExtend');
            });

            Route::group(['prefix' => 'crowd'], function() {
                Route::post('/', 'Request@storeCrowd');
                Route::post('/firework', 'Request@storeCrowdFirework');
                Route::post('/public', 'Request@storeCrowdPublic');
                Route::post('/campaign', 'Request@storeCrowdCampaign');
            });

            Route::group(['prefix' => 'hearse'], function() {
                Route::post('/', 'Request@storeHearse');
                Route::post('/other', 'Request@storeHearseOther');
            });

            Route::group(['prefix' => 'finger'], function() {
                Route::post('/new', 'Request@storeFingerNew');
            });

            Route::group(['prefix' => 'lost'], function() {
                Route::post('/new', 'Request@storeLostNew');
            });
        });

        /**
         * Section
        */
        Route::group(['prefix' => 'section'], function() {
            Route::get('/', 'Section@get');
        });

        /**
         * Unit
        */
        Route::group(['prefix' => 'unit'], function() {
            Route::get('/', 'Unit@get');
        });

        /**
         * Sector
        */
        Route::group(['prefix' => 'sector'], function() {
            Route::get('/', 'Sector@get');
        });

        /**
         * Feedback
        */
        Route::group(['prefix' => 'feedback'], function() {
            Route::get('/', 'Feedback@get');
            Route::post('/', 'Feedback@store');
        });

        /**
         * Queue
        */
        Route::group(['prefix' => 'queue'], function() {
            Route::post('/', 'Queue@store');
            Route::post('/call', 'Queue@call');
            Route::get('/attend', 'Queue@attend');
        });

        /**
         * Location
         */
        Route::group(['prefix' => 'location'], function() {
            Route::group(['prefix' => 'origin'], function() {
                Route::get('/regency', 'Location@getOriginRegency');
                Route::get('/district', 'Location@getOriginDistrict');
                Route::get('/village', 'Location@getOriginVillage');
            });

            Route::group(['prefix' => 'destination'], function() {
                Route::get('/regency', 'Location@getDestinationRegency');
                Route::get('/district', 'Location@getDestinationDistrict');
                Route::get('/village', 'Location@getDestinationVillage');
            });
        });
    }
);
