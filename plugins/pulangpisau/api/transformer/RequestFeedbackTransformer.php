<?php namespace Pulangpisau\Api\Transformer;

use League\Fractal\TransformerAbstract;

use Pulangpisau\Request\Models\RequestFeedback as RequestFeedbackModels;

class RequestFeedbackTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(RequestFeedbackModels $feedback)
    {
        return [
            'rate'    => $feedback->rate,
            'comment' => $feedback->comment,
            'created_at' => [
                'raw' => [
                    'l'     => $feedback->created_at->format('l'),
                    'd'     => $feedback->created_at->format('d'),
                    'F'     => $feedback->created_at->format('F'),
                    'Y'     => $feedback->created_at->format('Y'),
                    'H'     => $feedback->created_at->format('H'),
                    'i'     => $feedback->created_at->format('i'),
                ],
                'dFY'    => $feedback->created_at->format('d F Y'),
                'ldFY'   => $feedback->created_at->format('l, d F Y'),
                'ldFYHi' => $feedback->created_at->format('l, d F Y H:i'),
                'human'  => $feedback->created_at->diffForHumans()
            ]
        ];
    }
}
