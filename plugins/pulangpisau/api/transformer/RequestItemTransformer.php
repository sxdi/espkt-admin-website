<?php namespace Pulangpisau\Api\Transformer;

use Pulangpisau\Api\Transformer\RequestTransformer;
use Pulangpisau\Api\Transformer\RequestCustomerTransformer;
use Pulangpisau\Api\Transformer\RequestFeedbackTransformer;

use League\Fractal\TransformerAbstract;

use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

class RequestItemTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'request',
        'customer',
        'feedback',
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(RequestItemModels $item)
    {
        return [
            'id'      => $item->id,
            'name'    => $item->service_name,
            'kind'    => $item->kind,
            'channel' => $item->channel,
            'status'  => $item->status,
            'officer' => $item->user ? $item->user->officer->name ? $item->user->officer->name : $item->user->email : 'Tidak ada data'
        ];
    }


    /**
     *  Include Request
     * @return \League\Fractal\Resource\Item
     */
    public function includeRequest(RequestItemModels $item)
    {
        return $this->item($item->request, new RequestTransformer);
    }

    /**
     *  Include Customer
     * @return \League\Fractal\Resource\Item
     */
    public function includeCustomer(RequestItemModels $item)
    {
        if($item->customer) {
            return $this->item($item->customer, new RequestCustomerTransformer);
        }
    }

    /**
     *  Include Feedback
     * @return \League\Fractal\Resource\Item
     */
    public function includeFeedback(RequestItemModels $item)
    {
        if($item->feedback) {
            return $this->item($item->feedback, new RequestFeedbackTransformer);
        }
    }
}
