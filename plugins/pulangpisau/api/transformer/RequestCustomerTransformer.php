<?php namespace Pulangpisau\Api\Transformer;

use League\Fractal\TransformerAbstract;

use Pulangpisau\Request\Models\RequestCustomer as RequestCustomerModels;

class RequestCustomerTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(RequestCustomerModels $customer)
    {
        return [
            'name'        => $customer->name,
            'gender'      => $customer->gender,
            'job'         => $customer->job,
            'nationality' => $customer->nationality,
            'phone'       => $customer->phone,
            'email'       => $customer->email,
            'address'     => $customer->address,
        ];
    }
}
