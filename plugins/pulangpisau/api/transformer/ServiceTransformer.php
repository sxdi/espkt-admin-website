<?php namespace Pulangpisau\Api\Transformer;

use League\Fractal\TransformerAbstract;

use Pulangpisau\Service\Models\Service as ServiceModels;

class ServiceTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ServiceModels $service)
    {
        return [
            'id'          => $service->id,
            'name'        => $service->name,
            'code'        => $service->code,
            'day_open'    => $service->day_open,
            'time_open'   => $service->time_open,
            'description' => $service->description,
            'picture'     => $service->picture ? $service->picture->getPath() : false
        ];
    }
}
