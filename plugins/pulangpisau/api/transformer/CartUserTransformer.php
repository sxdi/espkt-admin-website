<?php namespace Cyberpos\Api\Transformer;

use Cyberpos\User\Models\User;

use League\Fractal\TransformerAbstract;

class CartUserTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'uid'   => $user->uid,
            'name'  => $user->name,
            'phone' => $user->phone,
        ];
    }
}
