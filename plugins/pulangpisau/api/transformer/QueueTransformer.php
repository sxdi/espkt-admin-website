<?php namespace Pulangpisau\Api\Transformer;

use League\Fractal\TransformerAbstract;

use Pulangpisau\Queue\Models\Queue as QueueModels;

class QueueTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(QueueModels $queue)
    {
        return [
            'counter'    => $queue->counter->code,
            'number'     => str_pad($queue->number, 3, '0', STR_PAD_LEFT),
            'created_at' => [
                'raw' => [
                    'l'     => $queue->created_at->format('l'),
                    'd'     => $queue->created_at->format('d'),
                    'F'     => $queue->created_at->format('F'),
                    'Y'     => $queue->created_at->format('Y'),
                    'H'     => $queue->created_at->format('H'),
                    'i'     => $queue->created_at->format('i'),
                ],
                'dFY'     => $queue->created_at->format('d F Y'),
                'ldFY'    => $queue->created_at->format('l, d F Y'),
                'ldFYHi'  => $queue->created_at->format('l, d F Y H:i'),
                'ldFYHis' => $queue->created_at->format('l, d F Y H:i:s'),
                'human'   => $queue->created_at->diffForHumans()
            ]
        ];
    }
}
