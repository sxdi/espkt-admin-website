<?php namespace Pulangpisau\Api\Transformer;

use League\Fractal\TransformerAbstract;

use Pulangpisau\Request\Models\Request as RequestModels;

class RequestTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(RequestModels $request)
    {
        return [
            'id'         => $request->id,
            'code'       => $request->code,
            'created_at' => [
                'raw' => [
                    'l'     => $request->created_at->format('l'),
                    'd'     => $request->created_at->format('d'),
                    'F'     => $request->created_at->format('F'),
                    'Y'     => $request->created_at->format('Y'),
                    'H'     => $request->created_at->format('H'),
                    'i'     => $request->created_at->format('i'),
                ],
                'dFY'    => $request->created_at->format('d F Y'),
                'ldFY'   => $request->created_at->format('l, d F Y'),
                'ldFYHi' => $request->created_at->format('l, d F Y H:i'),
                'human'  => $request->created_at->diffForHumans()
            ]
        ];
    }
}
