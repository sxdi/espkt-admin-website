@servers(['web' => 'u299535131@31.220.110.219 -p 65002'])

@task('build', ['on' => 'web'])
    cd domains
    cd polrespulpis.com
    cd public_html
    git pull
    php artisan cache:clear
@endtask

@task('build-plugins', ['on' => 'web'])
    cd ~/domains/polrespulpis.com/public_html
    git checkout master
    git pull
@endtask
