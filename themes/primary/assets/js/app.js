$(document).ready(function() {

    // Activate tooltip bootstrap
    $('[data-toggle=tooltip]').tooltip({
        container: 'body',
    });

    initDatepicker();

    initDatatable();
});

function initDatepicker() {
    $('.datepicker').daterangepicker({
        locale: {format: 'YYYY-MM-DD'},
        singleDatePicker: true,
    });
}

function initSelect2() {
    $('.select2').select2();
}

function initDatatable() {
    // Setup - add a text input to each footer cell
    $('.datatable tfoot th').each( function() {
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control" placeholder="Cari '+title+'" />' );
    } );

    // DataTable
    var table = $('.datatable').DataTable({'theme':'bootstrap'});

    // Apply the search
    table.columns().every( function () {
        var that = this;

        $('input', this.footer()).on('keyup change', function () {
            if(that.search() !== this.value) {
                that
                    .search( this.value )
                    .draw();
            }
        });
    });

    $('.datatable').DataTable().columns(-1).order('desc').draw();
}

function initSummernote() {
    $(".summernote-simple").summernote({
       dialogsInBody: true,
      minHeight: 150,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough']],
        ['para', ['paragraph']]
      ]
    });
}
